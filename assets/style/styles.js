import {StyleSheet, Dimensions} from 'react-native';
import {backGroundColor} from '../colors/colors';

const HEIGHT = Dimensions.get('window').height;

// login and signup style
export const userStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#009387',
    },
    header: {
        flex: 1,
        justifyContent: 'flex-end',
        paddingHorizontal: 20,
        paddingBottom: 50,
    },
    footer: {
        flex: 3,
        backgroundColor: '#fff',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingHorizontal: 20,
        paddingVertical: 30,
    },
    text_header: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 30,
    },
    text_footer: {
        color: '#05375a',
        fontSize: 18,
    },
    action: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5,
    },
    actionError: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#FF0000',
        paddingBottom: 5,
    },
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
    },
    errorMsg: {
        color: '#FF0000',
        fontSize: 14,
    },
    button: {
        alignItems: 'center',
        marginTop: 50,
    },
    signIn: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
    },
    textSign: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    textPrivate: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 20,
      },
      color_textPrivate: {
        color: 'grey',
      },
      
})

// index style
export const indexStyle = StyleSheet.create({
    btnView: {
        width: '85%', height: 100, backgroundColor: 'white', borderRadius: 10, alignSelf: 'center',
        shadowColor: backGroundColor, shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.23,
        shadowRadius: 2.62, elevation: 10, marginTop: 30, flexDirection: 'row'
    },
    currencyStyle:{
        width: 70, height: '90%',  alignSelf: 'center', justifyContent: 'center' ,alignItems: 'center'
    },
    showTextBtn: {
        width: 100, height: 40, borderRadius: 10, backgroundColor: backGroundColor, justifyContent: 'center', alignItems: 'center', marginLeft: 20
    }
})

// table style
export const tableStyle = StyleSheet.create({
    mainContainer: {
        flex: 1, backgroundColor: '#fff'
    },
    title: {
        fontSize: 15, fontWeight: 'bold', alignSelf: 'center'
    },
    value: {
        fontSize: 30, color: '#28C76F', fontWeight: 'bold'
    },
    header: { height: 50, },
    text: { marginLeft: 10, fontWeight: '100' },
    dataWrapper: {},
    row: { height: 40, backgroundColor: '#fff' },
    tableHead: {width: '100%', height: 120, borderBottomLeftRadius: 10, borderBottomRightRadius: 10, backgroundColor: '#fff', borderTopWidth: 0 }
})

// my profile style
export const profileStyle = StyleSheet.create({
    cardTitle: {
        margin: 10, fontSize: 18, fontWeight: 'bold'
    },
    infoCard: {
        width: '95%', height: HEIGHT/1.8, alignItems: 'center'
    },
    textInput: {
        width: '90%', height: 50, borderRadius: 5, borderWidth: 1, borderColor: '#ccc', paddingLeft: 10
    },
    updatebtn:{
        width: 100, height: 40, backgroundColor: backGroundColor, justifyContent: 'center', alignItems: 'center', marginTop: 20, borderRadius: 5
    }
})

export const transferStyle = StyleSheet.create( {
    cardStyle: {
        width: '95%', height: HEIGHT/4
    },
    titleView: {
        width: '100%', height: 70, backgroundColor: backGroundColor
    },
    titleStyle: {
        marginLeft: 20, marginTop: 20, fontSize: 20, fontWeight: 'bold'
    }
})