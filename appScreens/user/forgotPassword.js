/* eslint-disable prettier/prettier */
import React, { useState } from 'react';
import {View, Text, TouchableOpacity, TextInput, ActivityIndicator, StatusBar,} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import { userStyle } from '../../assets/style/styles';
import { API_URL } from '../../constants';


const ForgotPassword = ({ navigation }) => {

    const [userName, setUserName] = useState('')
    const [loading, setLoading] = useState(false)

    const forgotPass = () =>{
        if (!userName) {
            alert('Username is required')
        }else{
            setLoading(true)
            fetch(API_URL + 'forgot-password', {
                method: 'POST',
                 headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    username: userName,
                })
            })
    
                .then((response) => response.json())
                .then((responseJson) => {
                    alert(JSON.stringify(responseJson))
                    console.log(responseJson)
                    setLoading(false)
                    setUserName('')
                })
                .catch((error) => {
                    alert(error)
                    setLoading(false)
                    setUserName('')
                })
        }
    }

    return (
        <View style={userStyle.container}>
            <StatusBar backgroundColor="#009387" barStyle="light-content" />
            <View style={userStyle.header}>
                <Text style={userStyle.text_header}>Forgot Password</Text>
            </View>
            <Animatable.View animation="fadeInUpBig" style={userStyle.footer}>
                <Text style={userStyle.text_footer}>Username</Text>
                <View style={userStyle.action}>
                    <Icon name="ios-person" color="#05375a" size={20} />
                    <TextInput
                        placeholder="Your Username"
                        style={userStyle.textInput}
                        autoCapitalize="none"
                        value={userName}
                        onChangeText={(val) => setUserName(val)}
                    />
                </View>


                <View style={userStyle.button}>
                    <TouchableOpacity
                        style={userStyle.signIn}
                        onPress={() => {
                            forgotPass()
                        }}>
                        <LinearGradient
                            colors={['#08d4c4', '#01ab9d']}
                            style={userStyle.signIn}>
                            {loading === true ? <ActivityIndicator size='small' color='#fff' /> :
                                <Text
                                    style={[
                                        userStyle.textSign,
                                        {
                                            color: '#fff',
                                        },
                                    ]}>
                                    Forgot
                            </Text>}
                        </LinearGradient>
                    </TouchableOpacity>


                </View>
            </Animatable.View>
        </View>
    );

}

export default ForgotPassword;