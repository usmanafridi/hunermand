/* eslint-disable prettier/prettier */
import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    TextInput,
    ActivityIndicator,
    AsyncStorage,
    StatusBar,
    Alert,
    BackHandler
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import { userStyle } from '../../assets/style/styles';
import { API_URL } from '../../constants';
import { StackActions,NavigationActions } from 'react-navigation'
import { fullNameAtom, userIdAtom, userNameAtom, userTokenAtom} from '../recoil/atom';
import {useRecoilState} from 'recoil';

const Login = ({ navigation }) => {

    const [loginLoading, setLoginLoading] = React.useState(false)
    const [name, setName] = React.useState(null)
    const [password, setPassword] = React.useState(null)

    const [userName, setUserName] = useRecoilState(userNameAtom);
    const [fullName, setFullName] = useRecoilState(fullNameAtom);
    const [userId, setUserId] = useRecoilState(userIdAtom);
    const [token, setToken] = useRecoilState(userTokenAtom)

    const _storeUser = async (key, value) => {
        try {
          await AsyncStorage.setItem(key, value);
        } catch (error) {
        }
    }

    const loginHandle = () => {
        if (name === null || password === null) {
            Alert.alert(
                'Wrong Input!',
                'Username or password field cannot be empty.',
                [{ text: 'Okay' }],
            );
        }else {
            setLoginLoading(true)
            fetch(API_URL+'login', {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'

              },
              body: JSON.stringify({
                username: name,
                password: password,
              })
            })
      
              .then((response) => response.json())
              .then((responseData) => {
                if(responseData.message === 'Logged In Successfully'){
                    setUserName(responseData.user.pAds_userUsername)
                    setFullName(responseData.user.pAds_userfullName)
                    setUserId(responseData.user.pAds_userID)
                    setToken(responseData.token)
                    
                    let obj = {
                        auth: true,
                        userId: responseData.user.pAds_userID,
                        fullName: responseData.user.pAds_userfullName,
                        userName: responseData.user.pAds_userUsername,
                        token: responseData.token,
                      }
                      _storeUser('userAuth', JSON.stringify(obj))
                      setLoginLoading(false)

                      const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: 'home' })],
                      });
                      navigation.dispatch(resetAction);      
                }else{
                    alert('No data found')
                    setLoginLoading(false)
                }
                          
              })
              .catch((error) => {
                  alert(error)
                setLoginLoading(false)
              })
        }

    }
    

    return (
        <View style={userStyle.container}>
            <StatusBar backgroundColor="#009387" barStyle="light-content" />
            <View style={userStyle.header}>
                <Text style={userStyle.text_header}>Welcome!</Text>
            </View>
            <Animatable.View animation="fadeInUpBig" style={userStyle.footer}>
                <Text style={userStyle.text_footer}>Username</Text>
                <View style={userStyle.action}>
                    <Icon name="ios-person" color="#05375a" size={20} />
                    <TextInput
                        placeholder="Your Username"
                        style={userStyle.textInput}
                        autoCapitalize="none"
                        onChangeText={(val) => setName(val)}
                        // onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                    />
                </View>

                <Text
                    style={[
                        userStyle.text_footer,
                        {
                            marginTop: 35,
                        },
                    ]}>
                    Password
          </Text>
                <View style={userStyle.action}>
                    <Icon name="lock-closed" color="#05375a" size={20} />
                    <TextInput
                        placeholder="Your Password"
                        secureTextEntry={ true }
                        style={userStyle.textInput}
                        autoCapitalize="none"
                        onChangeText={(val) => setPassword(val)}
                    />
                   
                </View>

                <TouchableOpacity 
                onPress ={()=> navigation.navigate('forgot')}>
                    <Text style={{ color: '#009387', marginTop: 15 }}>
                        Forgot password?
            </Text>
                </TouchableOpacity>
                <View style={userStyle.button}>
                    <TouchableOpacity
                        style={userStyle.signIn}
                        onPress={() => {
                            loginHandle();
                        }}>
                        <LinearGradient
                            colors={['#08d4c4', '#01ab9d']}
                            style={userStyle.signIn}>
                            {loginLoading === true ? <ActivityIndicator size='small' color='#fff' /> :
                            <Text
                                style={[
                                    userStyle.textSign,
                                    {
                                        color: '#fff',
                                    },
                                ]}>
                                Sign In
                            </Text>}
                        </LinearGradient>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => navigation.navigate('signup')}
                        style={[
                            userStyle.signIn,
                            {
                                borderColor: '#009387',
                                borderWidth: 1,
                                marginTop: 15,
                            },
                        ]}>
                        <Text
                            style={[
                                userStyle.textSign,
                                {
                                    color: '#009387',
                                },
                            ]}>
                            Sign Up
              </Text>
                    </TouchableOpacity>
                </View>
            </Animatable.View>
        </View>
    );

}
export default Login;

