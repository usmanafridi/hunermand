import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Alert,
  ScrollView,
  StatusBar,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import { Dropdown } from 'react-native-material-dropdown-v2';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {userStyle} from './../../assets/style/styles';
import { API_URL } from '../../constants';

const SignUpScreen = ({navigation}) => {

    const [loading, setLoading] = React.useState(false)
    const [fullName, setFullName] = React.useState(null)
    const [userName, setUserName] = React.useState(null)
    const [password, setPassword] = React.useState(null)
    const [confirmPass, setConfirmPass] = React.useState(null)
    const [email, setEmail] = React.useState(null)
    const [city, setCity] = React.useState(null)
    const [country, setCountry] = React.useState(null)
    const [referral, setReferral] = React.useState(null)
    const [packagesArray, setPackagesArray] = React.useState([])
    const [packageId, setPackageId] = React.useState(0)

    React.useEffect(()=>{
        getPackages()
    },[])

    const getPackages = () => {

        fetch(API_URL + 'get-packages', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                const filter = []
                for (let i=0; i<responseJson.data.length; i++) {
                    if (responseJson.data[i].pAds_packageName === 'Standard') {

                    }else {
                        filter.push({
                            id: responseJson.data[i].pAds_packageID,
                            value: responseJson.data[i].pAds_packageName
                        })
                    }
                }
                setPackagesArray(filter)
            })
            .catch((error) => {
               
            })
    }

    const signUpHandle = () => {

        if (packageId === null || fullName === null || userName === null || password === null ||
            confirmPass === null || email === null || city === null || country === null || referral === null) {
           
            Alert.alert(
                'Wrong Input!',
                'Fields cannot be empty.',
                [{ text: 'Okay' }],
            );
        }else if (password != confirmPass) {
            Alert.alert(
                'Wrong Input!',
                'Password not matched',
                [{ text: 'Okay' }],
            );
        }
        else {
            setLoading(true)
            fetch(API_URL+'user-registration', {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                packageId: packageId,
                fullname: fullName,
                username: userName,
                email: email,
                city: city,
                country: country,
                referral: referral,
                password: password,
              })
            })
      
              .then((response) => response.json())
              .then((responseData) => {
                  setLoading(false)
                if(responseData.success === false){
                    alert('No free Leg for this Referral.')
                }else if (responseData.success === true){ 
                    alert('You are registered successfully Please Check your email for verification') 
                    navigation.goBack();  
   
                }else {
                  alert('Something went wrong')
                }
                          
              })
              .catch((error) => {
                setLoading(false)
              })
        }

    }

    const PackageId = (val) => {
        for (let i = 0; i < packagesArray.length; i++) {
            if (val === packagesArray[i].value) {
                setPackageId(packagesArray[i].id)
                break;
            }
        }
    }
  return (
    <View style={userStyle.container}>
      <StatusBar backgroundColor="#009387" barStyle="light-content" />
      <View style={userStyle.header}>
        <Text style={userStyle.text_header}>Register Now!</Text>
      </View>
      <Animatable.View animation="fadeInUpBig" style={userStyle.footer}>
        <ScrollView showsVerticalScrollIndicator={false}>
        <Text style={userStyle.text_footer}>Select Packages</Text>
        <Dropdown 
            onChangeText={(val)=>PackageId(val)}
            inputContainerStyle={{ borderBottomColor: 'transparent', backgroundColor: 'transparent' }} 
            label='Select Packages'
            data={packagesArray}

            />
       
       <Text style={[userStyle.text_footer,{marginTop: 35}]}>Full Name</Text>
          <View style={userStyle.action}>
            <Icon name="ios-person" color="#05375a" size={20} />
            <TextInput
              placeholder="Full Name"
              style={userStyle.textInput}
              autoCapitalize="none"
              onChangeText={(val)=>setFullName(val)}
            />
          </View>


          <Text style={[userStyle.text_footer,{marginTop: 35}]}>Username</Text>
          <View style={userStyle.action}>
            <Icon name="ios-person" color="#05375a" size={20} />
            <TextInput
              placeholder="Your Username"
              style={userStyle.textInput}
              autoCapitalize="none"
              onChangeText={(val)=>setUserName(val)}

            />
           
          </View>

          
          <Text
            style={[
              userStyle.text_footer,
              {
                marginTop: 35,
              },
            ]}>
            Password
          </Text>
          <View style={userStyle.action}>
            <Icon name="lock-closed" color="#05375a" size={20} />
            <TextInput
              placeholder="Your Password"
              secureTextEntry={true}
              style={userStyle.textInput}
              autoCapitalize="none"
              onChangeText={(val)=>setPassword(val)}

            />
            
          </View>

          <Text
            style={[
              userStyle.text_footer,
              {
                marginTop: 35,
              },
            ]}>
            Confirm Password
          </Text>
          <View style={userStyle.action}>
            <Icon name="lock-closed" color="#05375a" size={20} />
            <TextInput
              placeholder="Confirm Your Password"
              secureTextEntry={true}
              style={userStyle.textInput}
              autoCapitalize="none"
              onChangeText={(val)=>setConfirmPass(val)}

            />
          </View>

          <Text style={[userStyle.text_footer,{marginTop: 35}]}>Email</Text>
          <View style={userStyle.action}>
            <Icon name="mail" color="#05375a" size={20} />
            <TextInput
              placeholder="Email"
              style={userStyle.textInput}
              autoCapitalize="none"
              onChangeText={(val)=>setEmail(val)}

            />
          </View>

          <Text style={[userStyle.text_footer,{marginTop: 35}]}>City</Text>
          <View style={userStyle.action}>
            <FontAwesome5 name="city" color="#05375a" size={20} />
            <TextInput
              placeholder="City"
              style={userStyle.textInput}
              autoCapitalize="none"
              onChangeText={(val)=>setCity(val)}

            />
            
          </View>

          <Text style={[userStyle.text_footer,{marginTop: 35}]}>Country</Text>
          <View style={userStyle.action}>
            <Icon name="globe" color="#05375a" size={20} />
            <TextInput
              placeholder="Country"
              style={userStyle.textInput}
              autoCapitalize="none"
              onChangeText={(val)=>setCountry(val)}

            />
          </View>

          <Text style={[userStyle.text_footer,{marginTop: 35}]}>Referral</Text>
          <View style={userStyle.action}>
            <Icon name="ios-person" color="#05375a" size={20} />
            <TextInput
              placeholder="Referral"
              style={userStyle.textInput}
              autoCapitalize="none"
              onChangeText={(val)=>setReferral(val)}

            />
            
          </View>
          
          <View style={userStyle.textPrivate}>
            <Text style={userStyle.color_textPrivate}>
              By signing up you agree to our
            </Text>
            <Text style={[userStyle.color_textPrivate, {fontWeight: 'bold'}]}>
              {' '}
              Terms of service
            </Text>
            <Text style={userStyle.color_textPrivate}> and</Text>
            <Text style={[userStyle.color_textPrivate, {fontWeight: 'bold'}]}>
              {' '}
              Privacy policy
            </Text>
          </View>
          <View style={userStyle.button}>
            <TouchableOpacity style={userStyle.signIn} onPress={() => signUpHandle()}>
              <LinearGradient
                colors={['#08d4c4', '#01ab9d']}
                style={userStyle.signIn}>
                
                <Text
                  style={[
                    userStyle.textSign,
                    {
                      color: '#fff',
                    },
                  ]}>
                  { loading === true ? 'Loading...' : 'Sign Up' }
                </Text>
              </LinearGradient>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={[
                userStyle.signIn,
                {
                  borderColor: '#009387',
                  borderWidth: 1,
                  marginTop: 15,
                },
              ]}>
              <Text
                style={[
                  userStyle.textSign,
                  {
                    color: '#009387',
                  },
                ]}>
                Sign In
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </Animatable.View>
    </View>
  );
};

export default SignUpScreen;
