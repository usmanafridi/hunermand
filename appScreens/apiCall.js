import { API_URL } from '../constants';
import { AsyncStorage } from 'react-native';

export const getMyTotalEarnToday = (userId, setEarnToday, token) => {
    // const token = useRecoilValue(userTokenAtom)
    fetch(API_URL + 'get-user-today-earning/' + userId, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        },
    })

        .then((response) => response.json())
        .then((responseJson) => {
            if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                alert(responseJson.message)
                AsyncStorage.clear()
                navigation.navigate('login')
            }
            else if (responseJson.message === 'Record Found') {
                if (responseJson.data[0].amountEarnedToday === null) {
                    setEarnToday(0.00)
                } else {
                    setEarnToday(responseJson.data[0].amountEarnedToday)
                }
            }
        })

        .catch((error) => {

        })
}

export const getMainBalance = async (userId, setMainbalance, setWallet, token) => {

    await fetch(API_URL + 'get-user-main-balance/' + userId, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': token
        },
    })

        .then((response) => response.json())
        .then((responseJson) => {
            if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                alert(responseJson.message)
                AsyncStorage.clear()
                navigation.navigate('login')
            } else if (responseJson.message === 'Record Found') {
                setMainbalance(responseJson.data[0].pAds_mainBalance)
                setWallet(responseJson.data[0].pAds_wallet)
                setLoading(false)
                console.log('MAINBALNACE', responseJson)
                alert(JSON.stringify(responseJson))
            }
        })

        .catch((error) => {

        })
}

export const getInvestedBalance = async (userId, setInvestedBalance, token) => {

    await fetch(API_URL + 'get-user-invest-balance/' + userId, {
         method: 'GET',
         headers: {
             'Accept': 'application/json',
             'Content-Type': 'application/json',
             'Authorization': token
         },  
     })

         .then((response) => response.json())
         .then((responseJson) => {
             
             if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                 alert(responseJson.message)
                AsyncStorage.clear()
                navigation.navigate('login')
             } else if (responseJson.message === 'Record Found') {
                 setInvestedBalance(responseJson.data[0].pAds_userAmount)
             }
         })

         .catch((error) => {

         })
 }
 
export const getTotalEarning = async (userId, setEarnTotal, token) => {
     await fetch(API_URL + 'get-my-payment-total/' + userId, {
         method: 'GET',
         headers: {
             'Accept': 'application/json',
             'Content-Type': 'application/json',
             'Authorization': token
         },
     })

         .then((response) => response.json())
         .then((responseJson) => {
             if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                 alert(responseJson.message)
                AsyncStorage.clear()
                navigation.navigate('login')
             } else if (responseJson.message === 'Record Found') {
                 setEarnTotal(responseJson.data[0].pAds_userPaymentsTotal)
             }
         })

         .catch((error) => {

         })
 }
