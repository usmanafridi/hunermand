import React from 'react';
import {
  TextInput,
  View,
  StyleSheet
} from 'react-native';

const SearchBar = (props) => {
  return (
    <View style={[styles.textInputWrapper, props.customStyle]}>
      <TextInput
        placeholder={props.placeHolder}
        autoCapitalize="none"
        // placeholderTextColor="#742A88"
        keyboardType={props.keyboard}
        style={styles.inputStyle}
        value={props.value}
        onChangeText={(value) => {
          props.onChange(value);
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  textInputWrapper: {
    width: '90%',
    alignSelf: 'center',
    borderColor: '#B7B7B7',
    borderWidth: 1,
    height: 50,
    borderRadius: 6,
    paddingHorizontal: 10,
    margin: 20,
    backgroundColor: '#fff'
  },
  inputStyle: {
    textAlign: 'left',
    padding: Platform.OS === 'android' ? 13 : 18,
    fontSize: 12,
    width: '100%',
    color: '#000',
  },
});

export default SearchBar;