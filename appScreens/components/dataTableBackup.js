import React, { useState, useEffect } from 'react'
import { View, ScrollView, TextInput, TouchableOpacity } from 'react-native'
import { Table, Row } from 'react-native-table-component';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { tableStyle } from '../../assets/style/styles';

const DataTable = ({ navigation, rowData, ...props }) => {

    const [search, setSearch] = useState('')

    const _searchObject = () => {
        
        // for (let i=0; i<rowData.length; i++) {
        //     if (rowData[i] === search){
        //         alert('found')
        //     }else {
        //         alert(search)
        //     }
        // }
    }

    useEffect(()=> {
        _searchObject()
    },[search])

    return (

        <View style={tableStyle.mainContainer}>

            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                <View>
                    <View style={tableStyle.tableHead}>
                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                            <TouchableOpacity style={{ width: 30, justifyContent: 'center', alignItems: 'center' }}>
                                <Ionicons name='search' size={20} />
                            </TouchableOpacity>
                            <TextInput style={{ flex: 1, paddingLeft: 10 }} placeholder='Search here' 
                                onChangeText={(val)=> setSearch(val)}
                            />
                        </View>
                        <Table borderStyle={{}}>
                            <Row data={props.tableHead} widthArr={props.widthArr} style={tableStyle.header} textStyle={tableStyle.text} />
                        </Table>
                    </View>

                    <ScrollView style={tableStyle.dataWrapper}>
                        <Table borderStyle={{}}>
                            {
                                rowData.map((rowData, index) => (
                                    <Row
                                        key={index}
                                        data={rowData}
                                        widthArr={props.widthArr}
                                        style={[tableStyle.row, index % 2 && { backgroundColor: '#f9f9f9' }]}
                                        textStyle={tableStyle.text}
                                    />
                                ))
                            }
                        </Table>
                    </ScrollView>
                </View>
            </ScrollView>
        </View>
    )
}

export default DataTable;