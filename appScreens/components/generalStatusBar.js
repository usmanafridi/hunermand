
import React from 'react';
import { View, StatusBar, StyleSheet, Platform, TouchableOpacity, Text, Linking } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'

Ionicons.loadFont();

const GeneralStatusBarColor = ({ navigation, backgroundColor, iconName, ...props }) => (

<View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} barStyle = 'light-content' />
    <View style={styles.containerStyle}>
        <TouchableOpacity onPress={()=>
            iconName === 'menu' ?
            navigation.openDrawer():
            navigation.goBack()
            }>
            <Ionicons style={{marginLeft: 10}} size={30}  name={iconName} color = '#fff' />
        </TouchableOpacity>
        <Text style={{color:'#fff', fontSize:20, marginLeft: 20}}>{props.fullName}</Text>
    </View>
</View>
);

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 140 : 120;
const styles = StyleSheet.create({
statusBar: {
height: STATUSBAR_HEIGHT,
// flex:.15,
justifyContent: 'flex-end'
},
containerStyle: {
    width: '100%', height: 70, flexDirection: 'row', alignItems: 'center'
}
});

export default GeneralStatusBarColor;