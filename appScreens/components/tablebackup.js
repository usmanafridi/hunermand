import React, { Component } from 'react'
import { View, StyleSheet, ScrollView, TouchableOpacity } from 'react-native'
import { Text, Card } from 'native-base';
import { Table, Row } from 'react-native-table-component';


export default class DataTable extends Component {

    constructor(props) {
        super(props)

        this.state = {
            tableHead: ['Date', 'Amount'],
            widthArr: [200, 200,],
        }
    }

    render() {
        const state = this.state ;
        const tableData = [];

        for (let i = 0; i < 6; i += 1) {
            const rowData = [] ;
            for (let j = 0; j < 2; j += 1) {
                rowData.push(`${j}${i}`);
            }
            tableData.push(rowData);
        }
        return (

            <View style={styles.mainContainer}>

                <ScrollView horizontal={true} style={{ marginTop: 30 }} showsHorizontalScrollIndicator={false}>
                    <View>
                        <Card style={{ width: '100%', height: 50, borderBottomLeftRadius: 10, borderBottomRightRadius: 10, backgroundColor: '#fff', borderTopWidth: 0 }}>
                            <Table borderStyle={{}}>
                                <Row data={state.tableHead} widthArr={state.widthArr} style={styles.header} textStyle={styles.text} />
                            </Table>
                        </Card>


                        <ScrollView style={styles.dataWrapper}>
                            <Table borderStyle={{}}>
                                {
                                    tableData.map((rowData, index) => (
                                        <Row
                                            key={index}
                                            data={rowData}
                                            widthArr={state.widthArr}
                                            style={[styles.row, index % 2 && { backgroundColor: '#f9f9f9' }]}
                                            textStyle={styles.text}
                                        />
                                    ))
                                }
                            </Table>
                        </ScrollView>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    mainContainer: {
        flex: 1, backgroundColor: '#fff'
    },
    title: {
        fontSize: 15, fontWeight: 'bold', alignSelf: 'center'
    },
    value: {
        fontSize: 30, color: '#28C76F', fontWeight: 'bold'
    },
    header: { height: 50,  },
    text: { marginLeft:10, fontWeight: '100' },
    dataWrapper: {  },
    row: { height: 40, backgroundColor: '#fff' }
})