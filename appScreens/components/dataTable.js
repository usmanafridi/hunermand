import React, { useState, useEffect } from 'react';
import { DataTable } from 'react-native-paper';
import { View, ScrollView, Text, TouchableOpacity, ActivityIndicator, AsyncStorage, Dimensions } from 'react-native';
import { useRecoilValue, useRecoilState } from 'recoil';
import { userIdAtom, earnTodayAtom, userTokenAtom, mainBalanceAtom, investedBalanceAtom, earnTotalAtom, walletAtom } from '../recoil/atom';
import { getMainBalance, getMyTotalEarnToday, getTotalEarning, getInvestedBalance } from '../apiCall';
import { API_URL } from '../../constants';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

function Datatable({ header, datatable, screen, page = 1, perPage = 50, style }) {
  const [state, setState] = useState({
    page: page - 1,
    perPage,
    numberOfPages: Math.ceil(datatable.length / perPage),
  });

  const userId = useRecoilValue(userIdAtom)
 
  const [earnToday, setEarnToday] = useRecoilState(earnTodayAtom)
  const [mainBalance, setMainbalance] = useRecoilState(mainBalanceAtom)
  const [wallet, setWallet] = useRecoilState(walletAtom)
  const [investedBalance, setInvestedBalance] = useRecoilState(investedBalanceAtom)
  const [earnTotal, setEarnTotal] = useRecoilState(earnTotalAtom)

  const token = useRecoilValue(userTokenAtom)
  const [tableData, setTableData] = useState([])
  const [loading, setLoading] = useState(false)

  const mergedClick = (item) => {
      setLoading(true)
      fetch(API_URL + 'merged-user-click/' + item.mId + '/' + item.questionId, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': token
        },
      })

        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.success === false && responseJson.authMsg === 'token_error') {
            alert(responseJson.message)
           AsyncStorage.clear()
           navigation.navigate('login')
           setLoading(false)
        } else if (responseJson.resp_msg === 'limit_reached') {
            alert('Limit Reached! user has no more click')
            setLoading(false)
          }else {
            alert(JSON.stringify(responseJson))
            _getMergeAccounts()

            getMainBalance(userId, setMainbalance, setWallet, token)
            getMyTotalEarnToday(userId, setEarnToday, token)
            getTotalEarning(userId, setEarnTotal, token)
            getInvestedBalance(userId, setInvestedBalance, token)
            
            setLoading(false)
          }
         
        })

        .catch((error) => {
          console.log('Error', error)
        })
  }

  const _getMergeAccounts = () => {

    fetch(API_URL + 'get-user-merged-accounts/' + userId, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': token
      },
    })

      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.success === false && responseJson.authMsg === 'token_error') {
          alert(responseJson.message)
         AsyncStorage.clear()
         navigation.navigate('login')
      } else if (responseJson.message === 'Record Found') {
          let mArray = []
          for (let i = 0; i < responseJson.data.length; i++) {
            mArray.push({
              userName: responseJson.data[i].mUserName,
              mId: responseJson.data[i].mId,
              questionId: responseJson.questionId,
              clicksToday: responseJson.data[i].clicksToday,
              click: 'Click',
            })
          }
          setTableData(mArray)
          datatable = [...mArray]

        } else {
          setLoading(false)
          alert('No Record found')
        }

      })

      .catch((error) => {
        alert(error)
        setLoading(false)
      })
  }

  const getValue = (object, path) => {
    if (screen === 'Merged Account') {
      if (object.clicksToday === 6) {
        const new_obj = { ...object, ...object.click = 'User has no clicks' }
        return path
        .replace(/\[/g, '.')
        .replace(/\]/g, '')
        .split('.')
        .reduce((o, k) => (o || {})[k], new_obj);
      }else {
        return path
        .replace(/\[/g, '.')
        .replace(/\]/g, '')
        .split('.')
        .reduce((o, k) => (o || {})[k], object);
      }
    }else {
      return path
      .replace(/\[/g, '.')
      .replace(/\]/g, '')
      .split('.')
      .reduce((o, k) => (o || {})[k], object);
    }
  };

  return (
        <ScrollView>
            {loading ?
            <View style={{ width: WIDTH, height: HEIGHT, }}>
              <ActivityIndicator size='large' color='black' /> 
            </View>:
          <DataTable style={style}>
            <DataTable.Header backgroundColor={'#009387'}>
              {header.map((item, i) => {
                let sortDirection = item.sortDirection ? item.sortDirection : false;
                return (
                  <View style={{ width: 200, height: 50, justifyContent: 'center' }}>
                    <Text style={{ color: '#fff' }}>{item.headerTitle}</Text>
                    {/* <DataTable.Title
              key={i}
              sortDirection={sortDirection}>
              {item.headerTitle}
            </DataTable.Title> */}
                  </View>
                );
              })}
            </DataTable.Header>

            {datatable
              .slice(state.perPage * state.page, state.perPage * (state.page + 1))
              .map((item, i) => {
                return (
                  <View style={{ backgroundColor: i % 2 === 0 ? '#f8f8f8' : '#fff' }}>
                    <DataTable.Row key={i}>
                      {header.map((headerItem, j) => {
                        return (
                          <View style={{ width: 200, height: 50 }}>
                            {screen === 'Merged Account' ?
                              <View style={{ width: 200, height: 50 }}>
                                {headerItem.attr === 'click' &&
                                  item.clicksToday != 6 ?
                                  <TouchableOpacity style={{ width: 200, height: 50 }}
                                    onPress={() => mergedClick(item)}
                                  >
                                    <DataTable.Cell key={j}>
                                      {getValue(item, headerItem.attr)}
                                    </DataTable.Cell>
                                  </TouchableOpacity>
                                  :
                                  <View style={{ width: 200, height: 50, }}>
                                    <DataTable.Cell key={j}>
                                      {getValue(item, headerItem.attr)}
                                    </DataTable.Cell>
                                  </View>
                                  // :

                                  // <View style={{ width: 200, height: 50 }}>
                                  //   <DataTable.Cell key={j}>
                                  //     {getValue(item, headerItem.attr)}
                                  //   </DataTable.Cell>
                                  // </View>
                                }
                              </View> :

                              <DataTable.Cell key={j}>
                                {getValue(item, headerItem.attr)}
                              </DataTable.Cell>}
                          </View>
                        );
                      })}
                    </DataTable.Row>
                  </View>

                );
              })}

            <DataTable.Pagination
              page={state.page}
              numberOfPages={state.numberOfPages}
              onPageChange={page => {
                setState({ ...state, page });
              }}
              label={state.page + 1 + ' of ' + state.numberOfPages}
            />
          </DataTable>}
        </ScrollView>
  );
}

export default Datatable;