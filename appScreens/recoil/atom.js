import {atom} from 'recoil';

export const userIdAtom = atom({
    key: 'USERID',
    default: '0'
})

export const userTokenAtom = atom({
    key: 'TOKEN',
    default: '0'
})

export const userNameAtom = atom({
    key: 'USERNAME',
    default: 'Abc'
});

export const fullNameAtom = atom({
    key: 'FULLNAME',
    default: 'Abcd'
});

export const earnTodayAtom = atom({
    key: 'EARNTODAY',
    default: 0
})

export const mainBalanceAtom = atom({
    key: 'MAINBALANCE',
    default: 0
})

export const walletAtom = atom({
    key: 'WALLET',
    default: 0
})

export const investedBalanceAtom = atom({
    key: 'INVESTEDBALANCE',
    default: 0
})

export const earnTotalAtom = atom({
    key: 'EARNTOAL',
    default: 0
})

export const mergedAccountArrayAtom = atom({
    key: 'MERGEDACCOUNT',
    default: []
})

export const mergedAccountFullArrayAtom = atom({
    key: 'MERGEDACCOUNTFULL',
    default: []
})

export const mergedAccountLoadingAtom = atom({
    key: 'MERGEDACCOUNTLOADING',
    default: true
})
