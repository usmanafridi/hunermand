import React from 'react'
import { Animated, View, AsyncStorage, StyleSheet, } from 'react-native'
import { StackActions, NavigationActions } from 'react-navigation'
import { userNameAtom, fullNameAtom, userIdAtom, userTokenAtom, } from '../appScreens/recoil/atom';
import { useRecoilState, useRecoilValue } from 'recoil';

const Splash = ({ navigation }) => {

  // react hook
  const [opacity] = React.useState(new Animated.Value(0))

  // atom hook
  const [userName, setUserName] = useRecoilState(userNameAtom);
  const [fullName, setFullName] = useRecoilState(fullNameAtom);
  const [userId, setUserId] = useRecoilState(userIdAtom);
  const [token, setToken] = useRecoilState(userTokenAtom)

  React.useEffect(() => {
    // AsyncStorage.clear()
    _getStoregetData()
  }, [])


  const onLoad = () => {
    Animated.timing(opacity, {
      toValue: 1,
      duration: 3000,
      useNativeDriver: true,
    }).start();
    setTimeout(() => {
    }, 1000);
  }

  const _getStoregetData = async () => {
    try {
      const value = await AsyncStorage.getItem('userAuth');
      var res = JSON.parse(value);
      console.log("USER", res)
      if (value !== null) {

        if (res.auth === true) {
          
          setToken(res.token);
          setUserId(res.userId);
          setFullName(res.fullName);
          setUserName(res.userName);
          console.log("USER inside", res)

          const resetAction = StackActions.reset({
            index: 0,
            key: null,
            actions: [NavigationActions.navigate({ routeName: 'home' })],
          });
          navigation.dispatch(resetAction);
          // navigation.navigate('home')

        }

      } else {

        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'login' })],
        });
        navigation.dispatch(resetAction);
        // navigation.navigate('login')
      }
    }
    catch (error) {
      console.log("Exception =" + error);
    }
  }


  return (
    <View style={styles.container}>
      {/* <Animated.Image
        // onLoad={this.onLoad}
        source={require('../assets/images/logo.png')}

        style={[styles.image,
        {
          opacity: opacity,
          transform: [
            {
              scale: opacity.interpolate({
                inputRange: [0, 1],
                outputRange: [-4, 1.1],
              })
            },
          ],
        },
        navigation.style,
        ]}
      />
      <Animated.Text
        style={[styles.title,
        {
          opacity: opacity,
          transform: [
            {
              scale: opacity.interpolate({
                inputRange: [0, 1],
                outputRange: [-4, 1.1],
              })
            },
          ],
        },

        ]}>Welcome To Paymenta</Animated.Text> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: 250,
    height: 250,
    borderRadius: 10,
  },
  title: {
    marginTop: 20
  }
});

export default Splash;