import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, ScrollView, AsyncStorage, LogBox, StyleSheet} from 'react-native';
import { useRecoilState, useRecoilValue } from 'recoil';
import GeneralStatusBarColor from '../components/generalStatusBar';
import * as Animatable from 'react-native-animatable';
import { indexStyle } from '../../assets/style/styles';
import Dollar from 'react-native-vector-icons/Foundation';
import { fullNameAtom, userIdAtom, userNameAtom, earnTodayAtom, userTokenAtom, mainBalanceAtom, investedBalanceAtom, earnTotalAtom, walletAtom } from '../recoil/atom';
import { API_URL } from '../../constants';
import { backGroundColor } from '../../assets/colors/colors';
import { getMyTotalEarnToday, getMainBalance, getInvestedBalance, getTotalEarning } from '../apiCall';


const Home = ({ navigation }) => {
    const fullName = useRecoilValue(fullNameAtom)
    const userId = useRecoilValue(userIdAtom)
    const userName = useRecoilValue(userNameAtom)
    const token = useRecoilValue(userTokenAtom)
   

    // for global call
    const [earnToday, setEarnToday] = useRecoilState(earnTodayAtom)
    const [mainBalance, setMainbalance] = useRecoilState(mainBalanceAtom)
    const [wallet, setWallet] = useRecoilState(walletAtom)
    const [investedBalance, setInvestedBalance] = useRecoilState(investedBalanceAtom)
    const [earnTotal, setEarnTotal] = useRecoilState(earnTotalAtom)

    const [isComissionShow, setisComissionShow] = useState(false)
    const [comission, setComission] = useState('')
    const [comissionLoading, setComissionLoading] = useState(false)
    const [isSaleShow, setisSaleShow] = useState(false)
    const [sale, setSale] = useState('')
    const [saleLoading, setSaleLoading] = useState(false)
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        getMainBalance(userId, setMainbalance, setWallet, token)
        getInvestedBalance(userId, setInvestedBalance, token)
        getMyTotalEarnToday(userId, setEarnToday, token)
        getTotalEarning(userId, setEarnTotal, token)
        
        LogBox.ignoreLogs(['Animated: `useNativeDriver` was not specified.']);
        return () => {};
    },[]);

    const showComission = async () => {
        setComissionLoading(true)
       await fetch(API_URL + 'user-commissions/' + userId, {
            method: 'GET',
             headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.success === false){
                    alert(responseJson.message)
                    setComissionLoading(false)
                }
                else if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                   AsyncStorage.clear()
                   navigation.navigate('login')
                   setComissionLoading(false)
                } else if (responseJson.message === 'Record Found') {
                    setComission(responseJson.data)
                    setComissionLoading(false)
                    setisComissionShow(true)

                }
            })

            .catch((error) => {

            })
    }

    const showSale = async () => {
        setSaleLoading(true)
        await fetch(API_URL + 'get-total-sales-history/' + userName, {
            method: 'GET',
             headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                   AsyncStorage.clear()
                   navigation.navigate('login')
                } else if (responseJson.message === 'Record Found') {
                    setSale(responseJson.sales)
                    setSaleLoading(false)
                    setisSaleShow(true)
                }
            })

            .catch((error) => {

            })
    }
   
    const renderFirst = () => {
        return (
            <Animatable.View animation="fadeInUpBig" style={indexStyle.btnView}>
                <View style={{
                    flex: 1, justifyContent: 'center'
                }}>


                    <Text style={{ fontSize: 20, marginLeft: 20 }}>${parseFloat(mainBalance).toFixed(2)}</Text>
                    <Text style={{ color: 'grey', marginLeft: 20 }}>My rewards + salary balance</Text>

                </View>
                <View style={indexStyle.currencyStyle}>
                    <Dollar name='dollar' size={40} color='grey' />
                </View>
            </Animatable.View>
        )
    }

    const renderSecond = () => {
        return (
            <Animatable.View animation="fadeInUpBig" style={indexStyle.btnView}>
                <View style={{
                    flex: 1, justifyContent: 'center'
                }}>


                    <Text style={{ fontSize: 20, marginLeft: 20 }}>${parseFloat(investedBalance).toFixed(2)}</Text>
                    <Text style={{ color: 'grey', marginLeft: 20 }}>My invested balance</Text>

                </View>
                <View style={indexStyle.currencyStyle}>
                    <Dollar name='dollar' size={40} color='grey' />
                </View>
            </Animatable.View>
        )
    }

    const renderThird = () => {
        return (
            <Animatable.View animation="fadeInUpBig" style={indexStyle.btnView}>
                <View style={{
                    flex: 1, justifyContent: 'center'
                }}>


                    <Text style={{ fontSize: 20, marginLeft: 20 }}>${parseFloat(earnToday).toFixed(2)}</Text>
                    <Text style={{ color: 'grey', marginLeft: 20 }}>My total earning today</Text>

                </View>
                <View style={indexStyle.currencyStyle}>
                    <Dollar name='dollar' size={40} color='grey' />
                </View>
            </Animatable.View>
        )
    }

    const renderFourth = () => {
        return (
            <Animatable.View animation="fadeInUpBig" style={indexStyle.btnView}>
                <View style={{
                    flex: 1, justifyContent: 'center'
                }}>


                    <Text style={{ fontSize: 20, marginLeft: 20 }}>${parseFloat(earnTotal).toFixed(2)}</Text>
                    <Text style={{ color: 'grey', marginLeft: 20 }}>My total amount earned</Text>

                </View>
                <View style={indexStyle.currencyStyle}>
                    <Dollar name='dollar' size={40} color='grey' />
                </View>
            </Animatable.View>
        )
    }

    const renderFifth = () => {
        return (
            <Animatable.View animation="fadeInUpBig" style={indexStyle.btnView}>
                <View style={{
                    flex: 1, justifyContent: 'center'
                }}>

                    {!isComissionShow ? 
                    <TouchableOpacity style={indexStyle.showTextBtn}
                        onPress={()=>{
                            showComission()
                        }}
                    >
                        <Text style={{color: '#fff'}}>{comissionLoading ? 'Loading...':'Show'}</Text>
                    </TouchableOpacity> :
                    <Text style={{ fontSize: 20, marginLeft: 20 }} 
                        onPress={()=>{
                            setisComissionShow(false)
                        }}
                    >$ {parseFloat(comission).toFixed(2)}</Text>}
                    <Text style={{ color: 'grey', marginLeft: 20 }}>Commission</Text>

                </View>
                <View style={indexStyle.currencyStyle}>
                    <Dollar name='dollar' size={40} color='grey' />
                </View>
            </Animatable.View>
        )
    }

    const renderSixth = () => {
        return (
            <Animatable.View animation="fadeInUpBig" style={indexStyle.btnView}>
                <View style={{
                    flex: 1, justifyContent: 'center'
                }}>


                    {!isSaleShow ?
                        <TouchableOpacity style={indexStyle.showTextBtn}
                            onPress={()=>{
                                // setisSaleShow(true)
                                showSale()
                            }}
                        >
                            <Text style={{ color: '#fff' }}>Show</Text>
                        </TouchableOpacity> :
                        <Text style={{ fontSize: 20, marginLeft: 20 }}
                            onPress={()=>{
                                setisSaleShow(false)
                            }}
                        >$ {parseFloat(sale).toFixed(2)}</Text>}
                    <Text style={{ color: 'grey', marginLeft: 20 }}>Sale</Text>

                </View>
                <View style={indexStyle.currencyStyle}>
                    <Dollar name='dollar' size={40} color='grey' />
                </View>
            </Animatable.View>
        )
    }

    const renderSeventh = () => {
        return (
            <Animatable.View animation="fadeInUpBig" style={[indexStyle.btnView, {flexDirection: 'column'}]}>

                <View style={{flex: .7, backgroundColor: backGroundColor}}>
                    <Text style={styles.cardTitle}>My earning + commission balance</Text>
                </View>

                <View style={{flex: 1, marginTop: 5}}>
                    <Text style={{marginLeft: 20, marginTop: 10, fontSize: 20}}>${parseFloat(wallet).toFixed(2)}</Text> 
                </View>
                    
            </Animatable.View>
        )
    }

    const renderEighth = () => {
        return (
            <Animatable.View animation="fadeInUpBig" style={[indexStyle.btnView, {flexDirection: 'column'}]}>

                <View style={{flex: .7, backgroundColor: backGroundColor}}>
                    <Text style={styles.cardTitle}>My mine</Text>
                </View>

                <View style={{flex: 1, marginTop: 5}}>
                <TouchableOpacity style={indexStyle.showTextBtn}
                            onPress={()=>{
                                navigation.navigate('myMine')
                            }}
                        >
                            <Text style={{ color: '#fff' }}>Mine</Text>
                        </TouchableOpacity> 
                </View>
                    
            </Animatable.View>
        )
    }


    return (

        <View style={{ flex: 1, }}>
            <GeneralStatusBarColor backgroundColor='#009387' fullName={fullName} navigation={navigation} iconName='menu' ></GeneralStatusBarColor>
            <ScrollView style={{ flex: 1, }}>

                <View style={{ flex: 1, bottom: 10 }}>
                    {
                        renderFirst()
                    }
                    {
                        renderSecond()
                    }
                    {
                        renderThird()
                    }
                    {
                        renderFourth()
                    }
                    {
                        renderFifth()
                    }
                    {
                        renderSixth()
                    }
                    {
                        renderSeventh()
                    }
                    {
                        renderEighth()
                    }

                </View>
            </ScrollView>

        </View>

    )
}

const styles = StyleSheet.create({
    cardTitle: {
        marginLeft: 20, marginTop: 10, color: '#fff'
    }
})

export default Home;