import React, { useState, useEffect } from 'react';
import { View, ScrollView, Dimensions, Text, TouchableOpacity, Animated, AsyncStorage, StyleSheet } from 'react-native';
import GeneralStatusBarColor from '../components/generalStatusBar';
import { backGroundColor } from '../../assets/colors/colors';
import { useRecoilValue, useRecoilState } from 'recoil';
import { userIdAtom, earnTodayAtom, userTokenAtom, mainBalanceAtom, investedBalanceAtom, earnTotalAtom, walletAtom, } from '../recoil/atom';
import { API_URL } from '../../constants';
import { indexStyle } from '../../assets/style/styles';
import { CountdownCircleTimer } from 'react-native-countdown-circle-timer';
import { getMyTotalEarnToday, getMainBalance, getTotalEarning, getInvestedBalance } from '../apiCall';

const HEIGHT = Dimensions.get('window').height;

const MyMine = ({ navigation }) => {

    const userId = useRecoilValue(userIdAtom)
    const [earnToday, setEarnToday] = useRecoilState(earnTodayAtom)
    const [mainBalance, setMainbalance] = useRecoilState(mainBalanceAtom)
    const [wallet, setWallet] = useRecoilState(walletAtom)
    const [investedBalance, setInvestedBalance] = useRecoilState(investedBalanceAtom)
    const [earnTotal, setEarnTotal] = useRecoilState(earnTotalAtom)
    const token = useRecoilValue(userTokenAtom)

    const [countClick, setCountClick] = useState(0)
    const [condition, setCondition] = useState(false)
    const [counter, setCounter] = useState(10)
    const [date, setDate] = useState('')
    const [todayClicks, setTodayClicks] = useState(0)
    const [remainingClicks, setRemainingClicks] = useState(0)
    const [userType, setUserType] = useState('')
    const [questionId, setQuestionId] = useState(0)

    useEffect(() => {
        getTodayInfo()
        checkClicks()
    }, [])

    const getTodayInfo = () => {
        fetch(API_URL + 'todays-click-information/' + userId, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                    AsyncStorage.clear()
                    navigation.navigate('login')
                } else {
                    setDate(responseJson.data[0].date)
                    setTodayClicks(responseJson.data[0].countedRecords)
                    setRemainingClicks(responseJson.data[0].remainingRecords)

                }

                console.log('Tody info', responseJson)
            })

            .catch((error) => {
            })
    }

    const checkClicks = () => {
        fetch(API_URL + 'check-click-ads/' + userId, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                console.log('check clicks', responseJson)
                setUserType(responseJson.message)
                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                    AsyncStorage.clear()
                    navigation.navigate('login')
                } else if (responseJson.message === 'Multi click user') {
                    setCounter(20)
                }
                setQuestionId(responseJson.questionId)

                setCountClick(responseJson.countedClicks + 1)
            })

            .catch((error) => {

            })
    }

    const clickAdd = () => {
        setCondition(true)
        if (userType === 'Multi click user') {
            fetch(API_URL + 'multi-click-ad-user/' + userId + '/' + questionId, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            })

                .then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                        // alert(responseJson.message)
                        AsyncStorage.clear()
                        navigation.navigate('login')
                        setCondition(false)
                        console.log('RESPONSE', responseJson)
                        setCondition(false)
                    } else {
                        getTodayInfo()
                        checkClicks()
                        getMainBalance(userId, setMainbalance, setWallet, token)
                        getMyTotalEarnToday(userId, setEarnToday, token)
                        getTotalEarning(userId, setEarnTotal, token)
                        getInvestedBalance(userId, setInvestedBalance, token)
                    }
                })

                .catch((error) => {
                })
        } else {
            fetch(API_URL + 'single-click-ad-user/' + userId + '/' + questionId, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            })

                .then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                        alert(responseJson.message)
                        AsyncStorage.clear()
                        navigation.navigate('login')
                    } else {
                        getTodayInfo()
                        checkClicks()
                        getMyTotalEarnToday(userId, setEarnToday, token)
                    }
                })

                .catch((error) => {

                })
        }
    }

    return (
        <View style={{ flex: 1 }}>
            <GeneralStatusBarColor backgroundColor='#009387' fullName={'My mine'} navigation={navigation} iconName='arrow-back-sharp'></GeneralStatusBarColor>
            <ScrollView>
                <View style={{ width: '100%', height: HEIGHT / 2, alignItems: 'center', borderTopColor: '#fff', borderTopWidth: 3 }}>
                    <View style={{ width: '100%', height: 70, backgroundColor: backGroundColor }}>
                        <Text style={{ margin: 10, fontSize: 20, fontWeight: 'bold', color: '#fff' }}>Today's information</Text>
                    </View>

                    <View style={{ flexDirection: 'row', height: 70, width: '90%', borderColor: 'gray', borderWidth: .5, marginTop: 5 }}>
                        <View style={{ flex: 1, justifyContent: 'center', marginLeft: 20 }}>
                            <Text>Date</Text>
                        </View>

                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                            <View style={{ width: 150, height: 30, backgroundColor: backGroundColor, borderRadius: 5, marginRight: 10, justifyContent: 'center' }}>
                                <Text style={{ color: '#fff', alignSelf: 'center', fontSize: 16 }}>{date}</Text>
                            </View>
                        </View>
                    </View>

                    <View style={{ flexDirection: 'row', height: 70, width: '90%', borderColor: 'gray', borderWidth: .5, borderTopWidth: 0 }}>
                        <View style={{ flex: 1, justifyContent: 'center', marginLeft: 20, }}>
                            <Text>Per day clicks</Text>
                        </View>

                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                            <View style={{ width: 60, height: 30, backgroundColor: 'green', borderRadius: 5, marginRight: 10, justifyContent: 'center' }}>
                                <Text style={{ color: '#fff', alignSelf: 'center', fontSize: 16 }}>6</Text>
                            </View>
                        </View>
                    </View>

                    <View style={{ flexDirection: 'row', height: 70, width: '90%', borderColor: 'gray', borderWidth: .5, borderTopWidth: 0 }}>
                        <View style={{ flex: 1, justifyContent: 'center', marginLeft: 20 }}>
                            <Text>Clicks today</Text>
                        </View>

                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                            <View style={{ width: 60, height: 30, backgroundColor: backGroundColor, borderRadius: 5, marginRight: 10, justifyContent: 'center' }}>
                                <Text style={{ color: '#fff', alignSelf: 'center', fontSize: 16 }}>{todayClicks}</Text>
                            </View>
                        </View>
                    </View>

                    <View style={{ flexDirection: 'row', height: 70, width: '90%', borderColor: 'gray', borderWidth: .5, borderTopWidth: 0 }}>
                        <View style={{ flex: 1, justifyContent: 'center', marginLeft: 20 }}>
                            <Text>Clicks remaining</Text>
                        </View>

                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
                            <View style={{ width: 60, height: 30, backgroundColor: 'orange', borderRadius: 5, marginRight: 10, justifyContent: 'center' }}>
                                <Text style={{ color: '#fff', alignSelf: 'center', fontSize: 16 }}>{remainingClicks}</Text>
                            </View>
                        </View>
                    </View>
                </View>

                <View style={{ width: '100%', height: HEIGHT / 4, alignItems: 'center', borderTopColor: '#fff', borderTopWidth: 3 }}>
                    <View style={{ width: '100%', height: 70, backgroundColor: backGroundColor }}>
                        <Text style={{ margin: 10, fontSize: 20, fontWeight: 'bold', color: '#fff' }}>Today's click</Text>
                    </View>
                    <Text>click the below link if avaiable</Text>
                    {remainingClicks === 0 && condition === false ?
                        <Text style={{ marginTop: 30, color: 'red' }}>Your next clicks will available after 12am</Text> :
                        <TouchableOpacity style={indexStyle.showTextBtn}
                            onPress={() => {
                                clickAdd()
                            }}
                        >
                            <Text style={{ color: '#fff' }}>Click {countClick}</Text>
                        </TouchableOpacity>}
                </View>
            </ScrollView>

            {condition ?
                <View style={{ height: '100%', width: '100%', position: 'absolute', backgroundColor: 'transparent', justifyContent: 'center', alignItems: 'center' }}>
                    <CountdownCircleTimer
                        isPlaying
                        duration={counter}
                        colors="#004777"
                        onComplete={() => {
                            setCondition(false)
                            alert('Click success!')
                            return [true, 0]
                        }}
                    >
                        {({ remainingTime, animatedColor }) => (
                            <Animated.Text
                                style={{ ...styles.remainingTime, color: animatedColor }}>
                                {remainingTime}
                            </Animated.Text>
                        )}
                    </CountdownCircleTimer>
                </View> : null}

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ecf0f1',
        padding: 8,
    },
    remainingTime: {
        fontSize: 46,
    },
});


export default MyMine;