import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, Dimensions, ScrollView, AsyncStorage, Alert } from 'react-native';
import Accordion from 'react-native-collapsible/Accordion';
import { StackActions, NavigationActions } from 'react-navigation';
import {userNameAtom} from '../recoil/atom';
import {useRecoilValue} from 'recoil';

const HEIGHT = Dimensions.get('window').height

const SideDrawer = ({ navigation }) => {

    const userName = useRecoilValue(userNameAtom)
    const [activeSections, setActiveSection] = useState([])

    const SECTIONS = [
        {
            title: 'Dashboard',
            content: 'My Profile',
            content1: 'Change Password',
            content2: null,
            content3: null,
            icon: require('../../assets/images/dashboard.png')
        },
        {
            title: 'Accounts',
            content: 'Merge Accounts',
            content1: 'Merge Requests',
            content2: null,
            content3: null,
            icon: require('../../assets/images/account.png')

        },
        {
            title: 'Transactions',
            content: 'My Earnings',
            content1: 'My Sales',
            content2: 'My Comissions',
            content3: 'My Legs Sales',
            icon: require('../../assets/images/transaction.png')
        },
        {
            title: 'Send',
            content: 'To Members',
            content1: 'My Exchangers',
            content2: 'Member + Exchanger',
            content3: null,
            icon: require('../../assets/images/send.png')

        },
        {
            title: 'Transfer',
            content: 'Transfer Comission',
            content1: 'Transfer Earnings',
            content2: 'Re-Investments',
            content3: null,
            icon: require('../../assets/images/transfer2.png')


        },
        {
            title: 'Referral',
            content: 'Heirarchy',
            content1: 'Tree',
            content2: null,
            content3: null,
            icon: require('../../assets/images/referral.png')

        },
        {
            title: 'Rewards',
            content: 'My Rewards',
            content1: 'Rewards History',
            content2: 'My Salaries',
            content3: null,
            icon: require('../../assets/images/reward1.png')

        },
        {
            title: 'Reports',
            content: 'Payment History',
            content1: 'Reward+Salary History',
            content2: 'Reversal History',
            content3: 'Earning+Commission History',
            icon: require('../../assets/images/report.png')

        },
        {
            title: 'Upgrades',
            content: 'By Addons',
            content1: 'Update Package',
            content2: null,
            content3: null,
            icon: require('../../assets/images/upgrade.png')

        },
        {
            title: 'Logout',
            content: 'Logout',
            content1: null,
            content2: null,
            content3: null,
            icon: require('../../assets/images/cancel.png')

        },

    ];

    const _renderSectionTitle = section => {
        return (
            <View style={styles.content}>
                <Text>{section.content}</Text>

            </View>
        );
    };

    const _renderHeader = section => {
        return (
            <View style={styles.header}>
                <Image style={{ marginLeft: 10, width: 30, height: 30, alignSelf: 'center' }} source={section.icon} />
                <Text style={styles.headerText}>{section.title}</Text>
            </View>
        );
    };

    const _handleSection = (section) => {
        switch (section.content) {
            case 'My Profile':
                navigation.navigate('profile')
            break;

            case 'Merge Accounts':
                navigation.navigate('mergeAccount')
            break;

            case 'My Earnings':
                navigation.navigate('myEarnings')
            break;

            case 'To Members':
                navigation.navigate('sendTo')
            break;

            case 'Transfer Comission':
                navigation.navigate('tranferComission')
            break;

            case 'My Rewards':
                navigation.navigate('myReward')
            break;

            case 'Heirarchy':
                alert('Work in progress')
                // navigation.navigate('dataHeirarchy')
            break;

            case 'Logout':
            Alert.alert(
                "Logout",
                "Are you sure you want to logout",
                [
                  {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                  },
                  { text: "OK", onPress: () => {
                      AsyncStorage.clear()
                      const resetAction = StackActions.reset({
                          index: 0,
                          key: null,
                          actions: [NavigationActions.navigate({ routeName: 'login' })],
                      });
                      navigation.dispatch(resetAction);
                  }

                  }
                ],
                { cancelable: false }
              );
                
            break;

            case '':
            
            break;

            case '':
            
            break;

            default :

            break;
        }
    }

    const _handleSection1 = (section) => {
        switch (section.content1) {
            case 'Change Password':
                navigation.navigate('changePassword')
            break;

            case 'My Sales':
                navigation.navigate('mySales')
            break;

            case 'Merge Requests':
                navigation.navigate('mergeRequest')
            break;

            case 'My Exchangers':
                navigation.navigate('exchanger')
            break;

            case 'Transfer Earnings':
                navigation.navigate('transferEarning')
            break;

            case 'Reward+Salary History':
                navigation.navigate('rewardHistory')
            break;

            case 'Transaction History':
                navigation.navigate('transactionHistory')
            break;

            case 'Tree':
                navigation.navigate('dataTree')
            break;

            default :

            break;
        }
    }

    const _handleSection2 = (section) => {
        switch (section.content2) {
            case 'My Comission':
                navigation.navigate('myComission')
            break;

            case 'Re-Investments':
                navigation.navigate('reinvestment')
            break;

            case 'My Salaries':
                navigation.navigate('mySalary')
            break;

            case 'Reversal History':
                navigation.navigate('reversalHistory')
            break;

            case 'Member + Exchanger':
                navigation.navigate('memberPlusExchanger')
            break;

            default :

            break;
        }
    }

    const _handleSection3 = (section) => {
        switch (section.content3) {
            case 'My Legs Sales':
                navigation.navigate('legalSale')
            break;

            case 'Earning+Commission History':
                navigation.navigate('earningCommissionHistory')
            break;

            default :

            break;
        }
    }

    const _renderContent = section => {
        return (
            <View style={styles.contentView}>
                {section.content === null ? null :
                <TouchableOpacity onPress={()=>_handleSection(section)}>
                    <Text style={styles.contentText}>{section.content}</Text>
                </TouchableOpacity>}
                {section.content1 === null ? null :
                <TouchableOpacity onPress={()=>_handleSection1(section)}>
                    <Text style={styles.contentText}>{section.content1}</Text>
                </TouchableOpacity>}
                {section.content2 === null ? null :
                <TouchableOpacity onPress={()=>_handleSection2(section)}>
                    <Text style={styles.contentText}>{section.content2}</Text>
                </TouchableOpacity>}
                {section.content3 === null ? null :
                <TouchableOpacity onPress={()=>_handleSection3(section)}>
                    <Text style={styles.contentText}>{section.content3}</Text>
                </TouchableOpacity>}
            </View>
        );
    };

    const _updateSections = activeSections => {
        setActiveSection(activeSections)
    };

    return (
        <View style={{ flex: 1, }}>
            <View style={{ width: '100%', height: HEIGHT / 5, flexDirection: 'row', alignItems: 'center' }}>
                <Image style={{ width: 50, height: 50, marginLeft: 10 }} source={require('../../assets/images/logo.png')} />
                <Text style={{ marginLeft: 10, fontSize: 20, fontWeight: 'bold' }}>Payminta</Text>
            </View>
            {userName === 'Abc' ? null :
            <ScrollView style={{ flex: 1, }}>
                <Accordion
                    sections={SECTIONS}
                    activeSections={activeSections}
                    // renderSectionTitle={_renderSectionTitle}
                    touchableComponent={TouchableOpacity}
                    renderHeader={_renderHeader}
                    renderContent={_renderContent}
                    onChange={_updateSections}
                    
                />
            </ScrollView>}
        </View>
    )
}


const styles = StyleSheet.create({
    header: {
        marginLeft: 10, marginTop: 30, flexDirection: 'row',
    },
    headerText: {
        fontSize: 20, marginLeft: 10
    },
    contentView: {
        justifyContent: 'center', alignItems: 'center'
    },
    contentText: {
        fontSize: 12, fontWeight: 'bold', textAlign: 'justify', margin: 10, width: 130
    }
})

export default SideDrawer;
