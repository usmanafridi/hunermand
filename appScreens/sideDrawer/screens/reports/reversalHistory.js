import React, { useState, useEffect } from 'react';
import { View, ActivityIndicator, StyleSheet, AsyncStorage, ScrollView } from 'react-native';
import GeneralStatusBarColor from '../../../components/generalStatusBar';
import { useRecoilValue } from 'recoil';
import { fullNameAtom, userIdAtom, userTokenAtom } from '../../../recoil/atom';
import { DataTable, DataTableCell, DataTableRow, DataTablePagination } from 'material-bread';
import { API_URL } from '../../../../constants';
import SearchBar from '../../../components/searchBar';
import Datatable from '../../../components/dataTable';

const ReversalHistory = ({ navigation }) => {
    const fullName = useRecoilValue(fullNameAtom)
    const userId = useRecoilValue(userIdAtom);
    const token = useRecoilValue(userTokenAtom)

    const [search, setSearch] = useState(null)
    const [fullData, setFullData] = useState([])
    const [tableData, setTableData] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        _getReversalHistory()
    }, [loading]);

    const _getReversalHistory = () => {

        fetch(API_URL + 'get-reversal-history/' + userId, {
            method: 'GET',
             headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                   AsyncStorage.clear()
                   navigation.navigate('login')
                } else if (responseJson.message === 'Record Found') {
                    setTableData(responseJson.data)
                    setFullData(responseJson.data)
                    setLoading(false)
                } else {
                    setLoading(false)
                    alert('No Record found')
                }

            })

            .catch((error) => {
                alert(error)
                setLoading(false)
            })
    }

    const _searchItem = (searchItem) => {
        setSearch(searchItem)
        if (searchItem.length <= 1) {
            setTableData(fullData)
        }else {
            let searchArray = []
            tableData.filter((item)=>{
                if (item.pAds_userfullName.toLowerCase().includes(searchItem.toLowerCase())
                ||item.pAds_userUsername.toLowerCase().includes(searchItem.toLowerCase())
                ||item.pAds_userCountry.toLowerCase().includes(searchItem.toLowerCase())
                ||item.pAds_reversalToID.toString().includes(searchItem.toLowerCase())
                ||item.pAds_reversalFromID.toString().includes(searchItem.toLowerCase())
                ) {
                    searchArray.push(item)
                    setTableData(searchArray)
                }
            });
        }
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <GeneralStatusBarColor backgroundColor='#009387' fullName={'Reversal History'} navigation={navigation} iconName='arrow-back-sharp'></GeneralStatusBarColor>

            <SearchBar onChange={(val) => _searchItem(val)} placeHolder='Search here...' value={search} ></SearchBar>

            {loading ? 
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator size='large' color='green' />
                </View>
            :
            <ScrollView horizontal={true} style={styles.container}>
                <Datatable
                    header={[
                    { headerTitle: 'Reversal Amount', attr: 'pAds_reversalAmount' }, 
                    { headerTitle: 'Reversal Date', attr: 'pAds_reversalDate' }, { headerTitle: 'Package', attr: 'pAds_package' },
                    { headerTitle: 'Fullname', attr: 'pAds_userfullName'}, 
                    { headerTitle: 'Username', attr: 'pAds_userUsername'},
                    { headerTitle: 'Referer', attr: 'pAds_userReferer' },
                    // { headerTitle: 'Exchanger Id', attr: 'pAds_exchangerID' }, { headerTitle: 'Status', attr: 'status' },
                    { headerTitle: 'Exchanger', attr: 'pAds_IsExchanger' }, { headerTitle: 'Referred Approved', attr: 'pAds_referredApproved' }
                ]}
                    
                    datatable={tableData}
                    style={{ backgroundColor: '#fff', }}
                />
            </ScrollView>}

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ecf0f1',
        padding: 8,
    },
});

export default ReversalHistory;