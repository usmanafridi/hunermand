import React, { useState, useEffect, useCallback } from 'react';
import { View, ActivityIndicator, StyleSheet, AsyncStorage, ScrollView } from 'react-native';
import GeneralStatusBarColor from '../../../components/generalStatusBar';
import { useRecoilValue } from 'recoil';
import { fullNameAtom, userIdAtom, userTokenAtom } from '../../../recoil/atom';
import { API_URL } from '../../../../constants';
import SearchBar from '../../../components/searchBar';
import Datatable from '../../../components/dataTable';

const EarningCommissionHistory = ({ navigation }) => {
    const fullName = useRecoilValue(fullNameAtom)
    const userId = useRecoilValue(userIdAtom);
    const token = useRecoilValue(userTokenAtom)

    const [search, setSearch] = useState(null)
    const [fullData, setFullData] = useState([])
    const [tableData, setTableData] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        _getTransactionHistory()
    }, [loading]);

    const _getTransactionHistory = () => {

        fetch(API_URL + 'get-wallet-history/'+ userId, {
            method: 'GET',
             headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson)
                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                   AsyncStorage.clear()
                   navigation.navigate('login')
                } else if (responseJson.message === 'Record Found') {
                    setTableData(responseJson.data)
                    setFullData(responseJson.data)
                    setLoading(false)
                } else {
                    setLoading(false)
                    alert('No Record found')
                }
            })
            .catch((error) => {
                setLoading(false)
            })
    }

    const _searchItem = (searchItem) =>{
        setSearch(searchItem)
        if (searchItem.length <= 1) {
            setTableData(fullData)
        }else {
            let searchArray = []
            tableData.filter((item)=>{
                if (item.FromUser === null){

                }else if (item.ToUser === null){

                }
                else if (item.FromUser.toLowerCase().includes(searchItem.toLowerCase()) 
                || item.ToUser.toLowerCase().includes(searchItem.toLowerCase()))
                {
                    searchArray.push(item)
                    setTableData(searchArray)
                }
            })
        }
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <GeneralStatusBarColor backgroundColor='#009387' fullName={'Earning Plus Commission History'} navigation={navigation} iconName='arrow-back-sharp'></GeneralStatusBarColor>

            <SearchBar onChange={(val) => _searchItem(val)} placeHolder='Search here...' value={search} ></SearchBar>

            {loading ? 
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator size='large' color='green' />
                </View>
            :
            <ScrollView horizontal={true} style={styles.container}>
                <Datatable
                    header={[{ headerTitle: 'From user', attr: 'FromUser' }, { headerTitle: 'To user', attr: 'ToUser' },
                    { headerTitle: 'Transaction amount', attr: 'pAds_transactionAmount' }, , { headerTitle: 'Date', attr: 'pAds_transactionDateTime' }, 
                ]}
                    datatable={tableData}
                    style={{ backgroundColor: '#fff', }}
                />
            </ScrollView>}

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ecf0f1',
        padding: 8,
    },
});

export default EarningCommissionHistory;