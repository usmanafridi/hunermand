import React, { useState, useEffect } from 'react';
import { View, ActivityIndicator, ScrollView, AsyncStorage, StyleSheet } from 'react-native';
import GeneralStatusBarColor from '../../../components/generalStatusBar';
import { useRecoilValue } from 'recoil';
import { fullNameAtom, userIdAtom, userTokenAtom } from '../../../recoil/atom';
import { API_URL } from '../../../../constants';
import SearchBar from '../../../components/searchBar';
import Datatable from '../../../components/dataTable';

const MySalary = ({ navigation }) => {
    const fullName = useRecoilValue(fullNameAtom)
    const userId = useRecoilValue(userIdAtom);
    const token = useRecoilValue(userTokenAtom)

    const [search, setSearch] = useState(null)

    const [tableData, setTableData] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        _getMySalary()
    }, [loading]);

    const _getMySalary = () => {

        fetch(API_URL + 'get-my-salaries-history/'+userId, {
            method: 'GET',
             headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                console.log('Salary', responseJson)
                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                   AsyncStorage.clear()
                   navigation.navigate('login')
                } else if (responseJson.message === 'Record Found') {
                    setTableData(responseJson.data)
                    setLoading(false)
                }else {
                    setLoading(false)
                    alert('No Record found')
                }

            })

            .catch((error) => {
                alert(error)
                setLoading(false)
            })
    }

    const _searchItem = (item) => {
        setSearch(item)
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <GeneralStatusBarColor backgroundColor='#009387' fullName={'My Salary'} navigation={navigation} iconName='arrow-back-sharp'></GeneralStatusBarColor>

            <SearchBar onChange={(val) => _searchItem(val)} placeHolder='Search here...' value={search} ></SearchBar>

            {loading ? <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size='large' color='black' />
            </View> :
                <ScrollView horizontal={true} style={styles.container}>
                    <Datatable
                        header={[{ headerTitle: 'Reward id', attr: 'pAds_rewardID' }, { headerTitle: 'Reward name', attr: 'pAds_rewardName' },
                        { headerTitle: 'Reward rank', attr: 'pAds_rewardRank' }, { headerTitle: 'Total sale', attr: 'pAds_totalSale' }, 
                        { headerTitle: 'Salary', attr: 'pAds_salary' },
                        { headerTitle: 'Reward amount', attr: 'pAds_rewardAmount' }, { headerTitle: 'Reward image', attr: 'pAds_rewardImage' }]}

                        datatable={tableData}
                        style={{ backgroundColor: '#fff', }}
                    />
                </ScrollView>
            }
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ecf0f1',
        padding: 8,
    },
});

export default MySalary;
