import React, {useState, useEffect} from 'react';
import {View, ActivityIndicator, StyleSheet, AsyncStorage, ScrollView} from 'react-native';
import GeneralStatusBarColor from '../../../components/generalStatusBar';
import { useRecoilValue } from 'recoil';
import { fullNameAtom, userIdAtom, userTokenAtom } from '../../../recoil/atom';
import { DataTable, DataTableCell, DataTableRow, DataTablePagination } from 'material-bread';
import { API_URL } from '../../../../constants';
import SearchBar from '../../../components/searchBar';
import Datatable from '../../../components/dataTable';


const RewardHistory = ({navigation}) => {
    const fullName = useRecoilValue(fullNameAtom)
    const userId = useRecoilValue(userIdAtom);
    const token = useRecoilValue(userTokenAtom)

    const [search, setSearch] = useState(null)
            
    const [tableData, setTableData] = useState([])
    const [fullData, setFullData] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(()=> {
        _getMergeAccounts()
    },[loading]);

    const _getMergeAccounts = () => {

        fetch(API_URL + 'get-user-transaction-history/'+userId, {
            method: 'GET',
             headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                   AsyncStorage.clear()
                   navigation.navigate('login')
                } else if (responseJson.message === 'Record Found') {
                    setTableData(responseJson.data)
                    setFullData(responseJson.data)
                    setLoading(false)
                } else {
                    setLoading(false)
                    alert('No Record found')
                }

            })

            .catch((error) => {
                alert(error)
                setLoading(false)
            })
    }

    const _searchItem = (searchItem) =>{
        setSearch(searchItem)
        if (searchItem.length <= 1) {
            setTableData(fullData)
        }else {
            let searchArray = []
            tableData.filter((item)=>{
                if (item.FromUser === null){

                }else if (item.ToUser === null){

                }
                else if (item.FromUser.toLowerCase().includes(searchItem.toLowerCase()) 
                || item.ToUser.toLowerCase().includes(searchItem.toLowerCase()))
                {
                    searchArray.push(item)
                    setTableData(searchArray)
                }
            })
        }
    }

    return(
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <GeneralStatusBarColor backgroundColor='#009387' fullName={'Reward Plus Salary History'} navigation={navigation} iconName='arrow-back-sharp'></GeneralStatusBarColor>
            
            <SearchBar onChange={(val)=>_searchItem(val)} placeHolder='Search here...' value={search} ></SearchBar>
            
            {loading ? <View style={{flex:1, justifyContent:'center',alignItems:'center'}}>
                <ActivityIndicator size='large' color='black' />
            </View> : 
            <ScrollView horizontal={true} style={styles.container}>
            <Datatable
                header={[{ headerTitle: 'From user', attr: 'FromUser' }, { headerTitle: 'To user', attr: 'ToUser' }, 
                { headerTitle: 'Transaction amount', attr: 'pAds_transactionAmount' }, { headerTitle: 'Transacrion date', attr: 'pAds_transactionDateTime' },
                // { headerTitle: 'Salary', attr: 'pAds_salary' }, , { headerTitle: 'Reward amount', attr: 'pAds_rewardAmount' }, 
                // { headerTitle: 'Reward image', attr: 'pAds_rewardImage' }, , { headerTitle: 'Request id', attr: 'pAds_requestID' }, 
                // { headerTitle: 'User id', attr: 'pAds_userID' }, , { headerTitle: 'Reward status', attr: 'pAds_rewardStatus' },
                // {headerTitle: 'Reward Date',attr:'pAds_rewardDate'}, {headerTitle: 'Reward overriden',attr:'pAds_rewardOverridden'}, 
                // {headerTitle: 'Reward paid', attr:'pAds_rewardPaid'}
            ]}
                
                datatable={tableData}
                style={{ backgroundColor: '#fff', }}
            />
        </ScrollView>
        }
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ecf0f1',
        padding: 8,
    },
  });
export default RewardHistory;