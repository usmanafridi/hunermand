import React, { useState, useEffect } from 'react';
import { View, ActivityIndicator, FlatList, StyleSheet, AsyncStorage, TouchableOpacity } from 'react-native';
import { Text } from 'native-base';
import GeneralStatusBarColor from '../../../components/generalStatusBar';
import { useRecoilValue, useResetRecoilState } from 'recoil';
import { fullNameAtom, userIdAtom, userTokenAtom } from '../../../recoil/atom';
import { API_URL } from '../../../../constants';
import SearchBar from '../../../components/searchBar';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { backGroundColor } from '../../../../assets/colors/colors';

const MyRewards = ({ navigation }) => {

    const fullName = useRecoilValue(fullNameAtom)
    const userId = useRecoilValue(userIdAtom)
    const token = useRecoilValue(userTokenAtom)
    const [search, setSearch] = useState(null)
    const [rewardArray, setrewardArray] = useState([])
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        getReward()
    }, [])

    const getReward = () => {
        fetch(API_URL + 'get-rewards/' + userId, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                    AsyncStorage.clear()
                    navigation.navigate('login')
                } else {
                    setrewardArray(responseJson.data)
                    setLoading(false)
                    console.log('Reward', responseJson)
                }
            })

            .catch((error) => {
                alert(error)
                setLoading(false)
            })
    }
    const requestReward = (rewardId) => {
        fetch(API_URL + 'request-reward/' + userId + '/' + rewardId, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                    AsyncStorage.clear()
                    navigation.navigate('login')
                } else {
                    getReward()
                    alert(responseJson.message)
                }
            })

            .catch((error) => {
                alert(error)
                setLoading(false)
            })
    }

    const rewardStatus = (item) => {

        if (item.reward_status === 'not_eligble') {
            return <Text style={{ color: '#fff', textAlign: 'center' }}>Not Eligible</Text>
        } else if (item.reward_status === 'eligble' && item.overriden === 'eligbleAndoverridden') {
            return <Text style={{ color: '#fff', textAlign: 'center' }}>Request</Text>
        } else if (item.reward_status === 'eligble' && item.overriden === 'overridden') {
            return <Text style={{ color: '#fff', textAlign: 'center' }}>Overriden</Text>

        } else if (item.reward_status === 'received' && item.overriden === 'overridden') {
            return <Text style={{ color: '#fff', textAlign: 'center' }}>Received & Overriden</Text>
        } else if (item.reward_status === 'received' && item.overriden === 'eligbleAndoverridden') {

            return <Text style={{ color: '#fff', textAlign: 'center' }}>Received</Text>

        } else if (item.reward_status === 'time_remain' && item.overriden === 'eligbleAndoverridden') {

            return <Text style={{ color: '#fff', textAlign: 'center' }}>Time :{item.remainingDays}</Text>

        } else if (item.reward_status === 'time_remain' && item.overriden === 'overriden') {

            return <Text style={{ color: '#fff', textAlign: 'center' }}>Overriden</Text>

        } else if (item.reward_status === 'contact_admin' && item.overriden === 'eligbleAndoverridden') {
            return <Text style={{ color: '#fff', textAlign: 'center' }}>{item.timme_resp_Message}</Text>
        }
    }

    const changeStaus = (item) => {

        if (item.reward_status === 'not_eligble') {

        } else if (item.reward_status === 'eligble' && item.overriden === 'eligbleAndoverridden') {
            { requestReward(item.rewardId) }
        } else if (item.reward_status === 'eligble' && item.overriden === 'overriden') {

        } else if (item.reward_status === 'received' && item.overriden === 'overridden') {

        } else if (item.reward_status === 'received' && item.overriden === 'eligbleAndoverridden') {

        } else if (item.reward_status === 'time_remain' && item.overriden === 'eligbleAndoverridden') {

        } else if (item.reward_status === 'time_remain' && item.overriden === 'overriden') {

        } else if (item.reward_status === 'contact_admin' && item.overriden === 'eligbleAndoverridden') {

        }
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <GeneralStatusBarColor backgroundColor='#009387' fullName={'My Rewards'} navigation={navigation} iconName='arrow-back-sharp'></GeneralStatusBarColor>

            <SearchBar onChange={(val) => _searchItem(val)} placeHolder='Search here...' value={search} ></SearchBar>
            {loading ? <ActivityIndicator size='large' color='black' /> :
                <FlatList
                    data={rewardArray}
                    renderItem={({ item }) =>

                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <View style={{ width: '90%', }}>
                                <View style={{ width: '100%', height: 150, backgroundColor: '#ccc', justifyContent: 'center', alignItems: 'center' }}>
                                    <Ionicons name='person' size={100} />
                                </View>

                                <View style={{ flex: 1, backgroundColor: '#fff' }}>
                                    <Text style={{ margin: 20, fontWeight: 'bold' }}>Rank: Officer</Text>
                                    <Text note style={{ marginLeft: 20 }}>Rank: {item.rank}</Text>
                                    <Text note style={{ marginLeft: 20, marginTop: 20 }}>Total sale: ${item.totalSale}</Text>
                                    <Text note style={{ marginLeft: 20, marginTop: 20 }}>Salary: ${item.salary}</Text>
                                    <Text note style={{ marginLeft: 20, marginTop: 20 }}>Reward: ${item.reward}</Text>

                                    <Text note style={{ marginLeft: 20, marginTop: 20, color: 'green' }}>Leg wise sale (Mandatory): $200</Text>

                                    {item.legArr.map(item => {
                                        return (
                                            <View>
                                                <Text note style={{ marginLeft: 20, marginTop: 20 }}>Leg No : {item.legNumber}</Text>
                                                <Text note style={{ marginLeft: 20, marginTop: 20 }}>Legwise sale : ${item.legWiseSale}</Text>
                                            </View>
                                        )
                                    })}
                                    <TouchableOpacity style={{
                                        width: 120, height: 50,
                                        margin: 20, borderRadius: 10, backgroundColor: backGroundColor,
                                        justifyContent: 'center', alignItems: 'center'
                                    }}
                                        onPress={() => {
                                            changeStaus(item)
                                        }}
                                    >
                                        {rewardStatus(item)}
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>

                    }
                />}
        </View>
    )
}

export default MyRewards;