import React, { useState, useEffect } from 'react';
import { SafeAreaView, Text, View, AsyncStorage } from 'react-native';
import GeneralStatusBarColor from '../../../components/generalStatusBar';
import { userNameAtom, userTokenAtom } from '../../../recoil/atom';
import { useRecoilValue } from 'recoil';
import { API_URL } from '../../../../constants';
import Entypo from 'react-native-vector-icons/Entypo';
import TreeSelect from 'react-native-tree-select';

const DataTree = ({ navigation }) => {
  const userName = useRecoilValue(userNameAtom)
  const token = useRecoilValue(userTokenAtom)
  let [tree, setTree] = useState(
    [{
    id: '1',
    name: userName,
    parentId: '0'
  },
])
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    getTree(userName)
  }, [loading])

  const getTree = (name) => {
    fetch(API_URL + 'show-legs', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': token
      },
      body: JSON.stringify({
        username: name,
      })
    })

      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.success === false && responseJson.authMsg === 'token_error') {
          alert(responseJson.message)
         AsyncStorage.clear()
         navigation.navigate('login')
      } else {
        setLoading(false)
        let mArray = []
        let i = 1
        responseJson.data.forEach(element => {
          i = i+1 
          mArray.push({
            id: i,
            name: element.pAds_referralUsername,
            parentId: '0',
            children: []
          })
        });
        
        let combineArray = []
        combineArray.push({...tree[0] ,...{"children":mArray}})
        // delete combineArray[0].children;
        setTree(combineArray)
        
        console.log('Res', tree)
      }
      })
      .catch((error) => {
        alert(error)
        setLoginLoading(false)
      })
  }

  const updateTree = (name, currentNode) => {
    fetch(API_URL + 'show-legs', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': token
      },
      body: JSON.stringify({
        username: name,
      })
    })

      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.success === false && responseJson.authMsg === 'token_error') {
          alert(responseJson.message)
         AsyncStorage.clear()
         navigation.navigate('login')
      } else {
        let mArray = []
        let i = 3
        responseJson.data.forEach(element => {
          i = i+1 
          mArray.push({
            id: i,
            name: element.pAds_referralUsername,
            parentId: '0',
            children: []
          })
        });

        let combineArray = []
        search(tree[0],name, mArray);
        combineArray = [tree[0]] ;
        setTree(combineArray);
      }
      })
      .catch((error) => {
        // alert(error)
        setLoginLoading(false)
      })
  }
  
  const search = (subtree, target, mArray) => {
    if (subtree.name === target) {
      subtree.children = [...subtree.children, ...mArray];
      setTree(tree)
      // console.log("subtree",subtree );
      return subtree.name;
    }
    console.log('SUBTREE', subtree)
    for (const child of subtree.children) {
      const res = search(child, target,mArray);
      
      if (res) {
        return res;
      }
    }
  };


  const _onClickLeaf = ({ item, routes, currentNode }) => {
    updateTree(item.name, currentNode)
  };

  const _onClick = ({ item, routes, currentNode }) => {

  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <GeneralStatusBarColor backgroundColor='#009387' fullName={'Tree View'} navigation={navigation} iconName='arrow-back-sharp'></GeneralStatusBarColor>
      <View style={{ margin: 10, flex: 1, }}>
        {loading ? null :
        <TreeSelect
          data={tree}
          state = {this}
          // isOpen
          // openIds={['A01']}
          defaultSelectedId={['']}
          isShowTreeId={false}
          selectType="single"
          // selectType="multiple"
          itemStyle={{
            // backgroudColor: '#8bb0ee',
            fontSize: 15,
            color: '#000'
          }}
          selectedItemStyle={{
            // backgroudColor: '#f7edca',
            fontSize: 18,
            color: '#000',
            fontWeight: 'bold'
          }}
          onClick={_onClick}
          onClickLeaf={_onClickLeaf}
          treeNodeStyle={{
            openIcon: <Entypo
              resizeMode="stretch"
              size={20}
              name="arrow-down"
            />,
            closeIcon: <Entypo
              resizeMode="stretch"
              name="arrow-right"
              size={20}
            />
          }}
        />}

      </View>
    </SafeAreaView>
  );
};

export default DataTree;
