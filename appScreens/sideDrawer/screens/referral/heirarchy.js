import React, { useState, useEffect } from 'react';
import { StyleSheet, View,AsyncStorage, } from 'react-native';
import AwesomeHierarchyGraph from 'react-native-d3-tree-graph'
import GeneralStatusBarColor from '../../../components/generalStatusBar';
import { userNameAtom } from '../../../recoil/atom';
import { useRecoilValue } from 'recoil';
import { API_URL } from '../../../../constants';

var root = {
  name: "",
  id: 1,
  hidden: true,
  children: [

    {
      name: "anoop",
      id: 155,
      no_parent: true,
      children: [{
        name: "H",
        id: 8,
      }, {
        name: "I",
        id: 9,
      },
      {
        name: "I",
        id: 9,
      },
      {
        name: "I",
        id: 9,
      },
      {
        name: "I",
        id: 9,
      },
      {
        name: "I",
        id: 9,
      },
      {
        name: "I",
        id: 9,
      },
      {
        name: "I",
        id: 9,
      },
      {
        name: "I",
        id: 9,
      },
      {
        name: "I",
        id: 9,
      },
      {
        name: "I",
        id: 9,
      },
      {
        name: "I",
        id: 9,
        children: [{
          name: 'Usman',
          id: 10
        }]
      },

      ]
    },
  ]
}

var siblings = [{
  source: {
    id: 3,
    name: "C"
  },
  target: {
    id: 11,
    name: "K"
  }
}, {
  source: {
    id: 12,
    name: "L"
  },
  target: {
    id: 13,
    name: "J"
  }
}, {
  source: {
    id: 5,
    name: "D"
  },
  target: {
    id: 6,
    name: "E"
  }
}, {
  source: {
    id: 16,
    name: "Q"
  },
  target: {
    id: 10,
    name: "M"
  }
}];


const DataHeirarchy = ({ navigation }) => {
  const userName = useRecoilValue(userNameAtom)
  let [tree, setTree] = useState(
    {
      name: "",
      id: 1,
      hidden: true,
      children: [{
        name: "anoop",
        id: 155,
        no_parent: true,
        children: [{
          name: "H",
          id: 8,
        },
        ]
      },
      ]
    }
  )
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    // getTree(userName)
  }, [loading])

  const getTree = (name) => {
    fetch(API_URL + 'show-legs', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username: name,
      })
    })

      .then((response) => response.json())
      .then((responseJson) => {
        setLoading(false)
        let mArray = []
        let i = 1
        responseJson.data.forEach(element => {
          i = i + 1
          mArray.push({
            id: i,
            name: element.pAds_referralUsername,
            parentId: '0',
            children: []
          })
        });

        let combineArray = []
        combineArray.push({ ...tree[0], ...{ "children": mArray } })
        // delete combineArray[0].children;
        setTree(combineArray)

        console.log('Res', tree)

      })
      .catch((error) => {
        alert(error)
        setLoginLoading(false)
      })
  }

  const updateTree = (name, currentNode) => {
    fetch(API_URL + 'show-legs', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username: name,
      })
    })

      .then((response) => response.json())
      .then((responseJson) => {
        let mArray = []
        let i = 3
        responseJson.data.forEach(element => {
          i = i + 1
          mArray.push({
            id: i,
            name: element.pAds_referralUsername,
            parentId: '0',
            children: []
          })
        });

        let combineArray = []
        search(tree[0], name, mArray);
        combineArray = [tree[0]];
        setTree(combineArray);
      })
      .catch((error) => {
        // alert(error)
        setLoginLoading(false)
      })
  }

  const search = (subtree, target, mArray) => {
    if (subtree.name === target) {
      subtree.children = [...subtree.children, ...mArray];
      setTree(tree)
      // console.log("subtree",subtree );
      return subtree.name;
    }
    console.log('SUBTREE', subtree)
    for (const child of subtree.children) {
      const res = search(child, target, mArray);

      if (res) {
        return res;
      }
    }
  };

  return (
    <View style={styles.container}>
      <GeneralStatusBarColor backgroundColor='#009387' fullName={'Hierarchy View'} navigation={navigation} iconName='arrow-back-sharp'></GeneralStatusBarColor>
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <AwesomeHierarchyGraph
          root={root}
        //  siblings = {siblings}
        />
      </View>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },

});

export default DataHeirarchy;