import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    TextInput,
    Alert,
    ScrollView,
    StatusBar,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import { userStyle } from '../../../../assets/style/styles';
import { API_URL } from '../../../../constants';
import { useRecoilValue } from 'recoil';
import { userIdAtom, userTokenAtom } from '../../../recoil/atom';

const ChangePassword = ({ navigation }) => {

    const userId = useRecoilValue(userIdAtom)
    const token = useRecoilValue(userTokenAtom)

    const [loading, setLoading] = React.useState(false)
    const [newPassword, setNewPassword] = React.useState(null)
    const [oldPassword, setOldPassword] = React.useState(null)
    const [confirmPass, setConfirmPass] = React.useState(null)

    const changePassword = () => {
        if (oldPassword === null || newPassword === null || confirmPass === null) {
            Alert.alert(
                'Wrong Input!',
                'Fill the required fields',
                [{ text: 'Okay' }],
            );
        } else if (newPassword != confirmPass) {
            alert('Password not matched')
        } else {
            setLoading(true)
            fetch(API_URL + 'change-password', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
                body: JSON.stringify({
                    userId: userId,
                    oldPassword: oldPassword,
                    newPassword: newPassword
                })
            })

                .then((response) => response.json())
                .then((responseData) => {
                    alert(JSON.stringify(responseData))
                    console.log("Change password", responseData)
                    setOldPassword(null)
                    setNewPassword(null)
                    setConfirmPass(null)
                    setLoading(false)
                })
                .catch((error) => {
                    alert(error)
                    setLoading(false)
                    setOldPassword(null)
                    setNewPassword(null)
                    setConfirmPass(null)
                })
        }

    }


    return (
        <View style={userStyle.container}>
            <StatusBar backgroundColor="#009387" barStyle="light-content" />
            <View style={userStyle.header}>
                <Text style={userStyle.text_header}>Change Password</Text>
            </View>
            <Animatable.View animation="fadeInUpBig" style={userStyle.footer}>
                <ScrollView showsVerticalScrollIndicator={false}>

                    <Text
                        style={[
                            userStyle.text_footer,
                            {
                                marginTop: 35,
                            },
                        ]}>
                        Password
          </Text>
                    <View style={userStyle.action}>
                        <Icon name="lock-closed" color="#05375a" size={20} />
                        <TextInput
                            placeholder="Your Password"
                            secureTextEntry={true}
                            style={userStyle.textInput}
                            autoCapitalize="none"
                            value={oldPassword}
                            onChangeText={(val) => setOldPassword(val)}

                        />

                    </View>

                    <Text
                        style={[
                            userStyle.text_footer,
                            {
                                marginTop: 35,
                            },
                        ]}>
                        New Password
          </Text>
                    <View style={userStyle.action}>
                        <Icon name="lock-closed" color="#05375a" size={20} />
                        <TextInput
                            placeholder="New Password"
                            secureTextEntry={true}
                            style={userStyle.textInput}
                            autoCapitalize="none"
                            value={newPassword}
                            onChangeText={(val) => setNewPassword(val)}

                        />

                    </View>

                    <Text
                        style={[
                            userStyle.text_footer,
                            {
                                marginTop: 35,
                            },
                        ]}>
                        Confirm Password
          </Text>
                    <View style={userStyle.action}>
                        <Icon name="lock-closed" color="#05375a" size={20} />
                        <TextInput
                            placeholder="Confirm New Password"
                            secureTextEntry={true}
                            style={userStyle.textInput}
                            autoCapitalize="none"
                            value={confirmPass}
                            onChangeText={(val) => setConfirmPass(val)}

                        />
                    </View>


                    <View style={userStyle.button}>
                        <TouchableOpacity style={userStyle.signIn} onPress={() => changePassword()}>
                            <LinearGradient
                                colors={['#08d4c4', '#01ab9d']}
                                style={userStyle.signIn}>

                                <Text
                                    style={[
                                        userStyle.textSign,
                                        {
                                            color: '#fff',
                                        },
                                    ]}>
                                    {loading === true ? 'Loading...' : 'Change Password'}
                                </Text>
                            </LinearGradient>
                        </TouchableOpacity>


                    </View>
                </ScrollView>
            </Animatable.View>
        </View>
    );
};

export default ChangePassword;
