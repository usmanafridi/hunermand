import React, { useState, useEffect } from 'react';
import { View, ScrollView, TextInput, TouchableOpacity, AsyncStorage,Image } from 'react-native';
import { fullNameAtom, userIdAtom, userTokenAtom } from '../../../recoil/atom';
import { useRecoilValue } from 'recoil';
import GeneralStatusBarColor from '../../../components/generalStatusBar';
import { Card, Text } from 'native-base';
import { profileStyle } from '../../../../assets/style/styles';
import { API_URL } from '../../../../constants';

const Profile = ({ navigation }) => {
    const fullName = useRecoilValue(fullNameAtom)
    const userId = useRecoilValue(userIdAtom)
    const token = useRecoilValue(userTokenAtom)

    const [loading, setLoading] = useState(true)
    const [edtFullName, setEdtFullName] = useState('')
    const [edtEmail, setEdtEmail] = useState('')
    const [edtCountry, setEdtCountry] = useState('')
    const [edtCity, setEdtCity] = useState('')

    const [txtUserName, setTxtUserName] = useState('')
    const [txtSubDate, setTxtSubDate] = useState('')
    const [txtExpDate, setTxtExpDate] = useState('')
    const [txtRemaining, setTxtRemaining] = useState('')
    const [txtPackage, setTxtPackage] = useState('')
    const [txtAddView, setTxtAddView] = useState('')
    
    const [updateLoading, setUpdateLoading] = useState(false)

    useEffect(()=>{
        _getUserData()
        _getSubscription()
    },[loading])

    const _getUserData = () => {

        fetch(API_URL + 'get-my-profile/' + userId, {
            method: 'GET',
             headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                   AsyncStorage.clear()
                   navigation.navigate('login')
                } else if (responseJson.message === 'Record Found') {
                    setEdtFullName(responseJson.data[0].pAds_userfullName)
                    setTxtUserName(responseJson.data[0].pAds_userUsername)
                    setEdtEmail(responseJson.data[0].pAds_userEmail)
                    setEdtCountry(responseJson.data[0].pAds_userCountry)
                    // setEdtCity('Null')
                    setLoading(false)
                } else {
                    setLoading(false)
                    alert('No Record found')
                }

            })

            .catch((error) => {
                alert(error)
                setLoading(false)
            })
    }

    const _getSubscription = () => {

        fetch(API_URL + 'get-my-subscription/' + userId, {
            method: 'GET',
             headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                   AsyncStorage.clear()
                   navigation.navigate('login')
                } else if (responseJson.message === 'Record Found') {
                    setTxtSubDate(responseJson.data[0].sub[0].pAds_subscriptionDate)
                    setTxtExpDate(responseJson.data[0].sub[0].pAds_subscriptionExpiry)
                    setTxtRemaining(responseJson.data[0].sub[0].pAds_intervalinMonths)
                    setTxtPackage(responseJson.data[0].sub[0].pAds_packageName)
                    setTxtAddView(responseJson.data[0].totalClicks)
                    setEdtCity(responseJson.data[0].sub[0].pAds_userCity)

                } else {
                    
                    alert('No Record found')
                }

            })

            .catch((error) => {
                alert(error)
                setLoading(false)
            })
    }

    const _updateProfile = () => {
        setUpdateLoading(true)
        fetch(API_URL+'user-profile-update', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': token

            },
            body: JSON.stringify({
                userId: userId,
                fullName: edtFullName,
                email: edtEmail,
                country: edtCountry,
                city: edtCity
            })
          })
    
            .then((response) => response.json())
            .then((responseData) => {
                setUpdateLoading(false) 
                console.log('Update', responseData)
                alert(JSON.stringify(responseData))
            })
            .catch((error) => {
              setUpdateLoading(false)
            })
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <GeneralStatusBarColor backgroundColor='#009387' fullName={fullName} navigation={navigation} iconName='arrow-back-sharp'></GeneralStatusBarColor>

            <ScrollView style={{ flex: 1, }}>
                <View style={{ flex: 1, alignItems: 'center' }}>
                    <Card style={{ width: '95%', height: 60, }}>
                        <Text style={profileStyle.cardTitle}>Member Account Information</Text>
                    </Card>
                    <Card style={profileStyle.infoCard}>
                        <Text note style={{width: '90%', margin: 5}} >Full Name</Text>
                        <TextInput style={profileStyle.textInput}
                            onChangeText={(val)=>setEdtFullName(val)}
                            value = {edtFullName}
                        >

                        </TextInput>

                        <Text note style={{width: '90%', margin: 5}}>Email</Text>
                        <TextInput style={profileStyle.textInput} 
                            onChangeText={(val)=>setEdtEmail(val)}
                            value = {edtEmail}
                        >

                        </TextInput>

                        <Text note style={{width: '90%', margin: 5}}>Country</Text>
                        <TextInput style={profileStyle.textInput}
                            onChangeText={(val)=>setEdtCountry(val)}
                            value = {edtCountry}
                        >

                        </TextInput>

                        <Text note style={{width: '90%', margin: 5}}>City</Text>
                        <TextInput style={profileStyle.textInput}
                            onChangeText={(val)=>setEdtCity(val)}
                            value = {edtCity}
                        >

                        </TextInput>

                        <View style={{width: '90%'}}>
                           
                            <TouchableOpacity style={profileStyle.updatebtn}
                                onPress={()=> _updateProfile()}
                            >
                                <Text style={{color: '#fff', fontSize: 18, fontWeight: 'bold'}}>{!updateLoading ? 'Update':'Updating'}</Text>
                            </TouchableOpacity>
                        </View>
                    </Card>

                    <Card style={{ width: '95%', height: 60, }}>
                        <Text style={profileStyle.cardTitle}>My Profile</Text>
                    </Card>
                    <Card style={[profileStyle.infoCard, {alignItems: 'flex-start'}]}>
                    <View style={{width: '90%', justifyContent: 'center', alignItems: 'center', }}>
                            <Image style={{width: 100, height: 100, borderRadius: 100}} 
                                source={require('../../../../assets/images/logo.png')}
                            />
                        </View>

                        <View style={{width: '90%', margin: 15}}>
                            <Text style={{margin: 10}}>Fullname: {edtFullName}</Text>
                            <Text style={{margin: 10}}>Package:{txtPackage}</Text>
                            <Text style={{margin: 10}}>UserName: {txtUserName}</Text>
                            <Text style={{margin: 10}}>Email: {edtEmail}</Text>
                            <Text style={{margin: 10}}>Country: {edtCountry}</Text>
                        </View>
                    </Card>

                    <Card style={{ width: '95%', height: 60, }}>
                        <Text style={profileStyle.cardTitle}>My Subscriptions</Text>
                    </Card>
                    <Card style={[profileStyle.infoCard, {alignItems: 'flex-start'}]}>
                    <View style={{width: '90%', justifyContent: 'center', alignItems: 'center', }}>
                            <Image style={{width: 100, height: 100, borderRadius: 100}} 
                                source={require('../../../../assets/images/logo.png')}
                            />
                        </View>

                        <View style={{width: '90%', margin: 15}}>
                            <Text style={{margin: 10}}>Subscription date: {txtSubDate} </Text>
                            <Text style={{margin: 10}}>Expiry date: {txtExpDate}</Text>
                            <Text style={{margin: 10}}>Remaining: {txtRemaining}</Text>
                            <Text style={{margin: 10}}>Package: {txtPackage}</Text>
                            <Text style={{margin: 10}}>Total clicks: {txtAddView}</Text>
                        </View>
                    </Card>
                </View>
            </ScrollView>
        </View>
    )
}

export default Profile;