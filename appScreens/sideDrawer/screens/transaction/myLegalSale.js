import React, {useState, useEffect} from 'react';
import {View, ActivityIndicator,ScrollView,StyleSheet, AsyncStorage} from 'react-native';
// import DataTable, { Table } from '../../../components/dataTable';
import { useRecoilValue } from 'recoil';
import { fullNameAtom, userNameAtom, userTokenAtom } from '../../../recoil/atom';
import GeneralStatusBarColor from '../../../components/generalStatusBar';
import { API_URL } from '../../../../constants';
import Table from '../../../components/dataTable';
import { DataTable, DataTableCell, DataTableRow, DataTablePagination } from 'material-bread';
import SearchBar from '../../../components/searchBar';
import Datatable from '../../../components/dataTable';


const MyLegalSale = ({navigation}) => {
    
    const userName = useRecoilValue(userNameAtom);
    const fullName = useRecoilValue(fullNameAtom)
    const token = useRecoilValue(userTokenAtom)

    const [search, setSearch] = useState(null)
    const [tableData, setTableData] = useState([])
    const [fullData, setFullData] = useState([])
    const [loading, setLoading] = useState(true)

    const [page, setPage] = useState(0)
    const [perPage, setperPage] = useState(2)

    useEffect(()=> {
        _getLegalSales()
    },[loading]);

    const _getLegalSales = () => {

        fetch(API_URL + 'get-leg-sales-history/'+userName, {
            method: 'GET',
             headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                   AsyncStorage.clear()
                   navigation.navigate('login')
                } else if (responseJson.message === 'Record Found') {
                    // const rowData = [];
                    // for (let i = 0; i < responseJson.data.length; i += 1) {
                    //     rowData.push([
                    //         responseJson.data[i].leg, responseJson.data[i].total

                    //     ]
                    //     )
                    // }
                    setTableData(responseJson.data)
                    setFullData(responseJson.data)
                    setLoading(false)
                    console.log('Array console', rowData)
                } 

            })

            .catch((error) => {

            })
    }

    const _searchItem = (searchItem) => {
        setSearch(searchItem)
        if (searchItem.length <= 1) {
            setTableData(fullData)
        }else {
            let searchArray = []
        for (let i = 0; i < tableData.length; i++) {
            if (tableData[i].leg.toString().includes(searchItem)
            || tableData[i].total.toString().includes(searchItem)) {
                searchArray.push(tableData[i])
                setTableData(searchArray)
            }
        }
        }
    }

    return(
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <GeneralStatusBarColor backgroundColor='#009387' fullName={'Leg Sale'} navigation={navigation} iconName='arrow-back-sharp'></GeneralStatusBarColor>
            <SearchBar onChange={(val)=>_searchItem(val)} placeHolder='Search here...' value={search}></SearchBar>
            
            {loading ? <View style={{flex:1, justifyContent:'center',alignItems:'center'}}>
                <ActivityIndicator size='large' color='black' />
            </View> : 
           <ScrollView horizontal={true} style={styles.container}>
           <Datatable
               header={[{ headerTitle: 'Leg', attr: 'leg' }, { headerTitle: 'Total', attr: 'total' },
              
           ]}
               
               datatable={tableData}
               style={{ backgroundColor: '#fff', }}
           />
       </ScrollView>
        }
        </View>
    )
}
const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#ecf0f1',
      padding: 8,
  },
});

export default MyLegalSale;