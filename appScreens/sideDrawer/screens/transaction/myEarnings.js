import React, {useState, useEffect} from 'react';
import {View, ActivityIndicator, ScrollView, AsyncStorage} from 'react-native';
import Datatable from '../../../components/dataTable';
import { useRecoilValue } from 'recoil';
import { fullNameAtom, userIdAtom, userTokenAtom } from '../../../recoil/atom';
import GeneralStatusBarColor from '../../../components/generalStatusBar';
import { API_URL } from '../../../../constants';
import SearchBar from '../../../components/searchBar';


const MyEarnings = ({navigation}) => {
    const fullName = useRecoilValue(fullNameAtom);
    const userId = useRecoilValue(userIdAtom);
    const token = useRecoilValue(userTokenAtom)
    
    const [search, setSearch] = useState(null)
    const [tableHead, setTableHead] = useState(['Date', 'Amount']);
    const [widthArr, setWidth] = useState([200, 200,]);
    
    const [tableData, setTableData] = useState([])
    const [fullData, setFullData] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(()=> {
        _getEarning()
    },[loading]);

    const _getEarning = () => {

        fetch(API_URL + 'get-user-earning/' + userId, {
            method: 'GET',
             headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                   AsyncStorage.clear()
                   navigation.navigate('login')
                } else if (responseJson.message === 'Record Found') {
                    let mArray = []
                    responseJson.data.forEach(element => {
                        mArray.push({
                            pAds_dateTime: element.pAds_dateTime,
                            TotalEarning: element.TotalEarning.toFixed(2)
                        })
                    });
                    setTableData(mArray)
                    setFullData(mArray)
                    setLoading(false)
                }else {
                    setLoading(false)
                    alert('No Record found')
                }

            })

            .catch((error) => {

            })
    }

    const _searchItem = (searchItem) => {
        setSearch(searchItem)
        if (searchItem.length <= 1) {
            setTableData(fullData)
        }else {
            let searchArray = []
        for (let i = 0; i < tableData.length; i++) {
            if (tableData[i].pAds_dateTime.includes(searchItem)
            || tableData[i].TotalEarning.toString().includes(searchItem)) {
                searchArray.push(tableData[i])
                setTableData(searchArray)
            }
        }
        }
    }

    return(
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
           
            <GeneralStatusBarColor backgroundColor='#009387' fullName={'My Earnings'} navigation={navigation} iconName='arrow-back-sharp'></GeneralStatusBarColor>
            <SearchBar onChange={(val) => _searchItem(val)} placeHolder='Search here...' value={search}></SearchBar>

            {loading ?
                 <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size='large' color='black' />
                </View> :
                <ScrollView horizontal={true}>
                    <Datatable
                        header={[{ headerTitle: 'Date', attr: 'pAds_dateTime' }, { headerTitle: 'Amount', attr: 'TotalEarning' },
                        ]}

                        datatable={tableData}
                        style={{ backgroundColor: '#fff', }}
                    />
                </ScrollView>
            }
        </View>
    )
}

export default MyEarnings;