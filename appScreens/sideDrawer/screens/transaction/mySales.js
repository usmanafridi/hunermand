import React, { useState, useEffect } from 'react';
import { View, ActivityIndicator, ScrollView, AsyncStorage} from 'react-native';
import { useRecoilValue } from 'recoil';
import { fullNameAtom, userIdAtom, userNameAtom, userTokenAtom } from '../../../recoil/atom';
import GeneralStatusBarColor from '../../../components/generalStatusBar';
import { API_URL } from '../../../../constants';
import SearchBar from '../../../components/searchBar';
import Datatable from '../../../components/dataTable';


const MySales = ({ navigation }) => {
    const fullName = useRecoilValue(fullNameAtom);
    const userId = useRecoilValue(userIdAtom);
    const userName = useRecoilValue(userNameAtom)
    const token = useRecoilValue(userTokenAtom)

    const [search, setSearch] = useState(null)
    const [tableData, setTableData] = useState([])
    const [fullData, setFullData] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        _getSales()
    }, [loading]);

    const _getSales = () => {

        fetch(API_URL + 'get-user-sales/' + userName, {
            method: 'GET',
             headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                   AsyncStorage.clear()
                   navigation.navigate('login')
                } else if (responseJson.message === 'Record Found') {
                    setTableData(responseJson.data)
                    setFullData(responseJson.data)
                    setLoading(false)
                }else {
                    setLoading(false)
                    alert('No Record found')
                }
                console.log('Array console', responseJson)
            })

            .catch((error) => {

            })
    }

    const _searchItem = (searchItem) => {
        setSearch(searchItem)
        if (searchItem.length <= 1) {
            setTableData(fullData)
        }else {
            let searchArray = []
        for (let i = 0; i < tableData.length; i++) {
            if (tableData[i].pAds_referralSalesID.toString().includes(searchItem)
            || tableData[i].pAds_referralUsername.toString().includes(searchItem)
            || tableData[i].pAds_referredUsername.toString().includes(searchItem)) {
                searchArray.push(tableData[i])
                setTableData(searchArray)
            }
        }
        }
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <GeneralStatusBarColor backgroundColor='#009387' fullName={'My Sales'} navigation={navigation} iconName='arrow-back-sharp'></GeneralStatusBarColor>
            <SearchBar onChange={(val) => _searchItem(val)} placeHolder='Search here...' value={search}></SearchBar>

            {loading ?
                 <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size='large' color='black' />
                </View> :
                <ScrollView horizontal={true}>
                    <Datatable
                        header={[{ headerTitle: 'Referral sale id', attr: 'pAds_referralSalesID' }, { headerTitle: 'Referral username', attr: 'pAds_referralUsername' },
                        { headerTitle: 'Referred username', attr: 'pAds_referredUsername' }, , { headerTitle: 'User amount', attr: 'pAds_userAmount' },
                        { headerTitle: 'Level', attr: 'pAds_level' },
                        ]}

                        datatable={tableData}
                        style={{ backgroundColor: '#fff', }}
                    />
                </ScrollView>
            }

        </View>
    )
}

export default MySales;