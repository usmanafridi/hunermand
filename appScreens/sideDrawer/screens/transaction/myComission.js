import React, { useState, useEffect } from 'react';
import { View, ActivityIndicator, ScrollView, StyleSheet,AsyncStorage } from 'react-native';
import { useRecoilValue } from 'recoil';
import { fullNameAtom, userNameAtom, userTokenAtom } from '../../../recoil/atom';
import GeneralStatusBarColor from '../../../components/generalStatusBar';
import { API_URL } from '../../../../constants';
import SearchBar from '../../../components/searchBar';
import Datatable from '../../../components/dataTable';

const MyComission = ({ navigation }) => {
    const fullName = useRecoilValue(fullNameAtom);
    const userName = useRecoilValue(userNameAtom);
    const token = useRecoilValue(userTokenAtom)

    const [search, setSearch] = useState(null)
   
    const [tableData, setTableData] = useState([])
    const [fullData, setFullData] = useState([])
    const [loading, setLoading] = useState(true)


    useEffect(() => {
        _getComission()
    }, [loading]);

    const _getComission = () => {

        fetch(API_URL + 'get-user-commission/' + userName, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                // alert(JSON.stringify(responseJson))
                // if (responseJson.message === 'Record Found') {
                //     const rowData = [];
                //     for (let i = 0; i < responseJson.data.length; i += 1) {
                //         rowData.push([
                //             responseJson.data[i].pAds_commissionID, responseJson.data[i].pAds_commissionLevel,
                //             responseJson.data[i].pAds_referralUsername, responseJson.data[i].pAds_referredUsername,
                //             responseJson.data[i].pAds_userAmount_Previous, responseJson.data[i].pAds_userAmount_Increase,
                //             responseJson.data[i].pAds_commissionPercentage, responseJson.data[i].pAds_commissionAmount,
                //         ]
                //         )
                //     }
                //     setTableData(rowData)
                //     setLoading(false)
                // }
                // console.log('Array console', rowData)
                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                   AsyncStorage.clear()
                   navigation.navigate('login')
                } else if (responseJson.message === 'Record Found') {
                    setTableData(responseJson.data)
                    setFullData(responseJson.data)
                    setLoading(false)
                }else {
                    setLoading(false)
                    alert('No Record found')
                }
            })

            .catch((error) => {

            })
    }

    const _searchItem = (searchItem) => {
        setSearch(searchItem)
        if (searchItem.length <= 1) {
            setTableData(fullData)
        }else {
            let searchArray = []
        for (let i = 0; i < tableData.length; i++) {
            if (tableData[i].pAds_commissionID.toString().includes(searchItem)
            || tableData[i].pAds_referralUsername.toString().includes(searchItem)
            || tableData[i].pAds_referredUsername.toString().includes(searchItem)) {
                searchArray.push(tableData[i])
                setTableData(searchArray)
            }
        }
        }
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <GeneralStatusBarColor backgroundColor='#009387' fullName={'My Comission'} navigation={navigation} iconName='arrow-back-sharp'></GeneralStatusBarColor>
            <SearchBar onChange={(val) => _searchItem(val)} placeHolder='Search here...' value={search}></SearchBar>

            {loading ? <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size='large' color='black' />
            </View> :
                <ScrollView horizontal={true} style={styles.container}>
                    <Datatable

                        header={[{ headerTitle: 'Comission id', attr: 'pAds_commissionID' }, { headerTitle: 'Comission Level', attr: 'pAds_commissionLevel' },
                        { headerTitle: 'Referrel username', attr: 'pAds_referralUsername' }, { headerTitle: 'Referred username', attr: 'pAds_referredUsername' },
                        { headerTitle: 'Previous user amount', attr: 'pAds_userAmount_Previous' }, { headerTitle: 'Increase user amount', attr: 'pAds_userAmount_Increase' }, 
                        { headerTitle: 'Comission parcentage', attr: 'pAds_commissionPercentage' }, { headerTitle: 'Comission amount', attr: 'pAds_commissionAmount' }]}

                        datatable={tableData}
                        style={{ backgroundColor: '#fff', }}
                    />
                </ScrollView>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ecf0f1',
        padding: 8,
    },
  });

export default MyComission;