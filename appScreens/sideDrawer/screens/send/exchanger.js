import React, { useState, useEffect } from 'react';
import { View, ScrollView, TextInput, TouchableOpacity, AsyncStorage, Dimensions } from 'react-native';
import { fullNameAtom, userIdAtom, userTokenAtom, mainBalanceAtom, investedBalanceAtom, earnTotalAtom, earnTodayAtom, walletAtom } from '../../../recoil/atom';
import { useRecoilValue, useRecoilState } from 'recoil';
import GeneralStatusBarColor from '../../../components/generalStatusBar';
import { Card, Text } from 'native-base';
import { profileStyle } from '../../../../assets/style/styles';
import { API_URL } from '../../../../constants';
import { getMainBalance, getInvestedBalance, getMyTotalEarnToday, getTotalEarning } from '../../../apiCall';


const HEIGHT = Dimensions.get('window').height;

const Exchanger = ({ navigation }) => {
    const fullName = useRecoilValue(fullNameAtom)
    const userId = useRecoilValue(userIdAtom)
    const token = useRecoilValue(userTokenAtom)
   
    const [mainBalance, setMainbalance] = useRecoilState(mainBalanceAtom)
    const [wallet, setWallet] = useRecoilState(walletAtom)
    const [investedBalance, setInvestedBalance] = useRecoilState(investedBalanceAtom)
    const [earnTotal, setEarnTotal] = useRecoilState(earnTotalAtom)
    const [earnToday, setEarnToday] = useRecoilState(earnTodayAtom)

    const [findPayee, setPayee] = useState('')
    const [message, setMessage] = useState('')
    const [payeeName, setPayeeName] = useState('')

    const [returnmainBalance, setReturnMainBalance] = useState('')
    const [sendAmount, setSendAmount] = useState(0)
    const [pin, setPin] = useState('')
    const [loading, setLoading] = useState(false)
    

    useEffect(()=>{
        returnMainBalance()
    },[loading])

    const returnMainBalance = () =>{
        fetch(API_URL+'user-main-balance',{
            method: 'POST',
             headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
            body: JSON.stringify({
                userId: userId,
            })
        })
        .then((response)=> response.json())
        .then((responseJson)=> {
            if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                alert(responseJson.message)
               AsyncStorage.clear()
               navigation.navigate('login')
            } else {
                setReturnMainBalance(responseJson.data.balance)
            }

        }).catch((error)=> {
        })
    }

    const checkUser = () =>{
        if (!findPayee){
            alert('Registered payee name is required')
        }else {
        fetch(API_URL+'check-username-money-transfer-exchanger',{
            method: 'POST',
             headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
            body: JSON.stringify({
                userId: userId,
                username: findPayee
            })
        })
        .then((response)=> response.json())
        .then((responseJson)=> {
            console.log(responseJson)
            if (responseJson.message === 'No record Found') {
                setMessage('No user found')
            }else {
                setMessage('User found')
                console.log('USER FOUND', responseJson)
                setPayeeName(responseJson.data[0].pAds_userfullName)
            }

        }).catch((error)=> {
            alert(error)
        })
    }
    }

    const transferMoney = () =>{
        if(message === '' || message === 'No user found'){
            alert('Registered payee name is required')
        }
        else if (sendAmount === '0' || sendAmount === '' || pin === ''){
            alert('Please enter required information')
        }
        else{
            setLoading(true)
            fetch(API_URL+'transfer-money-user',{
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
                body: JSON.stringify({
                    userId: userId,
                    userFullName: fullName,
                    userAmount: sendAmount,
                    payeeUsername: findPayee,
                    userTpin: pin
                })
            })
            .then((response)=> response.json())
            .then((response)=> {
                alert(JSON.stringify(response.message))
                returnMainBalance()

                setPayee('')
                setMessage('')
                setPayeeName('')
                setSendAmount('')
                setPin('')

                // global function
                getMainBalance(userId, setMainbalance, setWallet, token)
                getInvestedBalance(userId, setInvestedBalance,token)
                getMyTotalEarnToday(userId, setEarnToday, token)
                getTotalEarning(userId, setEarnTotal, token)

            }).catch((error)=> {
                alert(error)
                setLoading(false)
            }) 
        }
    }


    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <GeneralStatusBarColor backgroundColor='#009387' fullName={'My Exchanger'} navigation={navigation} iconName='arrow-back-sharp'></GeneralStatusBarColor>

            <ScrollView style={{ flex: 1, }}>
                <View style={{ flex: 1, alignItems: 'center' }}>
                    <Card style={{ width: '95%', height: 60, }}>
                        <Text style={[profileStyle.cardTitle, { fontSize: 16 }]}>Send money (to current or other exchanger) 3% transaction charges apply</Text>
                    </Card>
                    <Card style={[profileStyle.infoCard, { height: HEIGHT / 1.4 }]}>
                        <Text note style={{ width: '90%', margin: 5 }}>Exchanger name</Text>
                        <TextInput style={profileStyle.textInput}
                            placeholder='Exchanger name'
                            value={findPayee}
                            onChangeText={(name)=>setPayee(name)}
                        />
                        <View style={{ width: '90%', flexDirection: 'row' }}>
                            <TouchableOpacity style={profileStyle.updatebtn}
                                onPress={()=>checkUser()}
                            >
                                <Text style={{ color: '#fff', fontSize: 18, fontWeight: 'bold' }}>Check</Text>
                            </TouchableOpacity>
                            <Text style={{alignSelf: 'center', marginTop: 20, marginLeft: 20, fontSize: 14, color: message === 'No user found' ? 'red' : 'green'}}>
                                {message}
                            </Text>
                        </View>

                        <Text note style={{ width: '90%', margin: 5 }}>Full name</Text>
                        <TextInput style={profileStyle.textInput}
                            placeholder='Full name'
                            editable={false}
                            value={payeeName}
                        />

                        <Text note style={{ width: '90%', margin: 5 }}>Sending amount</Text>
                        <TextInput style={profileStyle.textInput}
                            placeholder='Amount'
                            value={sendAmount}
                            onChangeText={(value)=>setSendAmount(value)}
                        />

                        <Text note style={{ width: '90%', margin: 5 }}>Your total amount</Text>
                        <TextInput style={profileStyle.textInput}
                            placeholder='Total amount'
                            value={parseFloat(returnmainBalance).toFixed(2)}
                            editable={false}
                        />

                        <Text note style={{ width: '90%', margin: 5 }}>Tpin</Text>
                        <TextInput style={profileStyle.textInput}
                            placeholder='Tpin'
                            value={pin}
                            onChangeText={(val)=>setPin(val)}
                            secureTextEntry
                        />

                        <View style={{ width: '90%' }}>
                            <TouchableOpacity style={profileStyle.updatebtn}
                                onPress={()=>transferMoney()}
                            >
                                <Text style={{ color: '#fff', fontSize: 18, fontWeight: 'bold' }}>{
                                    loading ? 'sending' : 'Send'
                                }</Text>
                            </TouchableOpacity>
                        </View>
                    </Card>

                </View>
            </ScrollView>
        </View>
    )
}

export default Exchanger;