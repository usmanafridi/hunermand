import React, {useState, useEffect} from 'react';
import {View, ActivityIndicator,StyleSheet, AsyncStorage, ScrollView} from 'react-native';
import GeneralStatusBarColor from '../../../components/generalStatusBar';
import { useRecoilValue, useRecoilState } from 'recoil';
import { fullNameAtom, userIdAtom, userTokenAtom, mergedAccountFullArrayAtom, mergedAccountLoadingAtom } from '../../../recoil/atom';
import { DataTable, DataTableCell, DataTableRow, DataTablePagination } from 'material-bread';
import { API_URL } from '../../../../constants';
import SearchBar from '../../../components/searchBar';
import Datatable from '../../../components/dataTable';

const MergeAccount = ({navigation}) => {
    const fullName = useRecoilValue(fullNameAtom)
    const userId = useRecoilValue(userIdAtom);
    const token = useRecoilValue(userTokenAtom)

    const [search, setSearch] = useState(null)
    const [tableData, setTableData] = useState([])
    const [fullData, setFullData] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(()=> {
        _getMergeAccounts()
    },[]);

    const _getMergeAccounts = () => {

        fetch(API_URL + 'get-user-merged-accounts/'+userId, {
            method: 'GET',
             headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {

                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                   AsyncStorage.clear()
                   navigation.navigate('login')
                } else if (responseJson.message === 'Record Found') {
                    let mArray = []
                    for (let i=0; i<responseJson.data.length; i++) {
                        mArray.push({
                            userName: responseJson.data[i].mUserName,
                            mId: responseJson.data[i].mId,
                            questionId: responseJson.questionId,
                            clicksToday: responseJson.data[i].clicksToday,
                            click: 'Click',
                        })
                    }
                    setLoading(false)
                    setTableData(mArray)
                    setFullData(mArray)
                    console.log('MERGE ACCOUNT 88888', tableData)
                }else {
                    setLoading(false)
                    alert('No Record found')
                }

            })

            .catch((error) => {
                alert(error)
                setLoading(false)
            })
    }

    const _searchItem = (searchItem) => {
        setSearch(searchItem)
        if (searchItem.length <= 1) {
            setTableData(fullData)
        }else {
            let searchArray = []
        for (let i = 0; i < tableData.length; i++) {
            if (tableData[i].userName.toLowerCase().includes(searchItem.toLowerCase())) {
                searchArray.push(tableData[i])
                setTableData(searchArray)
            }
        }
        }
    }

    return(
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <GeneralStatusBarColor backgroundColor='#009387' fullName={'Merged Account'} navigation={navigation} iconName='arrow-back-sharp'></GeneralStatusBarColor>
            
            <SearchBar onChange={(val)=>_searchItem(val)} placeHolder='Search here...' value={search} ></SearchBar>
            
            {loading ? <View style={{flex:1, justifyContent:'center',alignItems:'center'}}>
                <ActivityIndicator size='large' color='black' />
            </View> : 
            
            <ScrollView horizontal={true} style={styles.container}>
            <Datatable
                header={[{ headerTitle: 'Merged username', attr: 'userName' }, { headerTitle: 'Clicks', attr: 'click' },]}
                datatable={tableData}
                screen = 'Merged Account'
                style={{ backgroundColor: '#fff', }}
            />
        </ScrollView>
        }
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ecf0f1',
        padding: 8,
    },
});

export default MergeAccount;