import React, {useState, useEffect} from 'react';
import {View, ActivityIndicator,StyleSheet, AsyncStorage, ScrollView} from 'react-native';
import GeneralStatusBarColor from '../../../components/generalStatusBar';
import { useRecoilValue } from 'recoil';
import { fullNameAtom, userNameAtom, userTokenAtom } from '../../../recoil/atom';
import { DataTable, DataTableCell, DataTableRow, DataTablePagination } from 'material-bread';
import { API_URL } from '../../../../constants';
import SearchBar from '../../../components/searchBar';
import Datatable from '../../../components/dataTable';


const MergeRequest = ({navigation}) => {
    const fullName = useRecoilValue(fullNameAtom)
    const userName = useRecoilValue(userNameAtom);
    const token = useRecoilValue(userTokenAtom)
    const [search, setSearch] = useState(null)

    const [tableData, setTableData] = useState([])
    const [fullData, setFullData] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(()=> {
        _getMergeRequest()
    },[loading]);

    const _getMergeRequest = () => {

        fetch(API_URL + 'get-user-merged-requests/'+userName, {
            method: 'GET',
             headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                   AsyncStorage.clear()
                   navigation.navigate('login')
                } else if (responseJson.message === 'Record Found') {
                    setTableData(responseJson.data)
                    setFullData(responseJson.data)
                    setLoading(false)
                    console.log('Array console', rowData)
                } 

            })

            .catch((error) => {

            })
    }

    const _searchItem = (searchItem) => {
        setSearch(searchItem)
        if (searchItem.length <= 1) {
            setTableData(fullData)
        }else {
            let searchArray = []
        for (let i = 0; i < tableData.length; i++) {
            if (tableData[i].pAds_referralUsername.toLowerCase().includes(searchItem.toLowerCase())
            || tableData[i].pAds_referredUsername.toLowerCase().includes(searchItem.toLowerCase())) {
                searchArray.push(tableData[i])
                setTableData(searchArray)
            }
        }
        }
    }


    return(
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <GeneralStatusBarColor backgroundColor='#009387' fullName={'Merged Request'} navigation={navigation} iconName='arrow-back-sharp'></GeneralStatusBarColor>
            <SearchBar onChange={(val)=>_searchItem(val)} placeHolder='Search by referred username and referral username...' value={search}></SearchBar>

            {loading ? <View style={{flex:1, justifyContent:'center',alignItems:'center'}}>
                <ActivityIndicator size='large' color='black' />
            </View> : 
            <ScrollView horizontal={true} style={styles.container}>
            <Datatable
                header={[{ headerTitle: 'Comission id', attr: 'pAds_commissionID' }, { headerTitle: 'Comission level', attr: 'pAds_commissionLevel' },
                { headerTitle: 'Referral username', attr: 'pAds_referralUsername' }, , { headerTitle: 'Referred username', attr: 'pAds_referredUsername' }, 
                { headerTitle: 'Previoud user amount', attr: 'pAds_userAmount_Previous' }, , { headerTitle: 'User amount', attr: 'pAds_userAmount' }, 
                { headerTitle: 'Increase user amount', attr: 'pAds_userAmount_Increase' }, , { headerTitle: 'Comission percentage', attr: 'pAds_commissionPercentage' }, 
                {headerTitle: 'Comission amount',attr:'pAds_commissionAmount'}
            ]}
                
                datatable={tableData}
                style={{ backgroundColor: '#fff', }}
            />
        </ScrollView>
        }
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ecf0f1',
        padding: 8,
    },
});

export default MergeRequest;