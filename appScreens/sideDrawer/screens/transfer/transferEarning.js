import React, {useState, useEffect} from 'react';
import {View, TouchableOpacity, AsyncStorage, ActivityIndicator} from 'react-native';
import GeneralStatusBarColor from '../../../components/generalStatusBar';
import SearchBar from '../../../components/searchBar';
import {Card, Text} from 'native-base';
import {fullNameAtom, userIdAtom, userTokenAtom} from '../../../recoil/atom';
import {useRecoilValue} from 'recoil';
import {transferStyle, indexStyle} from '../../../../assets/style/styles';
import { API_URL } from '../../../../constants';

const TransferEarning = ({navigation}) =>{
    
    const userId = useRecoilValue(userIdAtom)
    const token = useRecoilValue(userTokenAtom)

    const [mainBalance, setMainbalance] = useState(0)
    const [wallet, setWallet] = useState(0)
    const [total, setTotal] = useState(0)
    const [loading, setLoading] = useState(false)

    useEffect(()=>{
        getMainBalance()
        getTotal()
    })

    const getTotal = () =>{
        fetch(API_URL + 'get-my-payment-total/' + userId, {
            method: 'GET',
             headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                   AsyncStorage.clear()
                   navigation.navigate('login')
                } else if (responseJson.message === 'Record Found') {
                    setTotal(responseJson.data[0].pAds_userPaymentsTotal)
                }
            })

            .catch((error) => {

            })
    }

    const getMainBalance = () => {

        fetch(API_URL + 'get-user-main-balance/' + userId, {
            method: 'GET',
             headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                   AsyncStorage.clear()
                   navigation.navigate('login')
                } else if (responseJson.message === 'Record Found') {
                    setMainbalance(responseJson.data[0].pAds_mainBalance)
                    setWallet(responseJson.data[0].pAds_wallet)
                }
            })

            .catch((error) => {

            })
    }

    const transferComission = () => {
        setLoading(true)
        fetch(API_URL+'transfer-earning-to-main-balance',{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
            body: JSON.stringify({
                userId: userId,
            })
        })
        .then((response)=> response.json())
        .then((responseJson)=> {
            if (responseJson.message === true) {
                alert(JSON.stringify(responseJson))
                getMainBalance()
                getTotal()
                setLoading(false)
            }else {
                alert(JSON.stringify(responseJson))
                setLoading(false)
            }
        }).catch((error)=> {
            alert(error)
            setLoading(false)
        }) 
    }


    return(
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <GeneralStatusBarColor backgroundColor='#009387' fullName={'Transfer Earning'} navigation={navigation} iconName='arrow-back-sharp'></GeneralStatusBarColor>

            <View style={{flex: 1, alignItems: 'center'}}>
                <Card style={transferStyle.cardStyle}>
                    <View style={transferStyle.titleView}>
                        <Text style={{margin: 20, fontWeight: 'bold', color: '#fff'}}>My reward + salary balance</Text>
                    </View>
                    <View style={{flex: 1}}>
                        <Text style={transferStyle.titleStyle}>Reward + salary balance</Text>
                        <Text note style={{marginLeft: 20}}>You have ${parseInt(mainBalance).toFixed(2)} reward + salary balance</Text>
                    </View>
                </Card>

                <Card style={transferStyle.cardStyle}>
                <View style={transferStyle.titleView}>
                        <Text style={{margin: 20, fontWeight: 'bold', color: '#fff'}}>My total earning</Text>
                    </View>
                    <View style={{flex: 1}}>
                        <Text style={transferStyle.titleStyle}>Total earnings</Text>
                        <Text note style={{marginLeft: 20}}>Your total earning is ${total.toFixed(2)}</Text>
                        {/* <Text style={{marginLeft: 20}}>Nothing to transfer</Text> */}
                        {
                            total <= 0 ? null : 
                            <TouchableOpacity style={[indexStyle.showTextBtn,
                             {alignSelf: 'flex-end', marginRight: 20, bottom: 0, marginTop: 5, width: 300}]}
                                onPress={()=>transferComission()}
                             >
                                {loading ? <ActivityIndicator color = '#fff' /> :
                                <Text style={{color: '#fff'}}>Transfer to earning + commissions</Text>}
                            </TouchableOpacity>
                        }
                    </View>
                </Card>

                <Card style={transferStyle.cardStyle}>
                <View style={transferStyle.titleView}>
                        <Text style={{margin: 20, fontWeight: 'bold', color: '#fff'}}>My earning + commission balance</Text>
                    </View>
                    <View style={{flex: 1}}>
                        <Text style={transferStyle.titleStyle}>Earning + comission balance</Text>
                        <Text note style={{marginLeft: 20}}> ${parseFloat(wallet).toFixed(2)}</Text>

                    </View>
                </Card>
            </View>
        </View>
    )
}

export default TransferEarning;