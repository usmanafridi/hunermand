import React, {useState} from 'react';
import {View, Dimensions, TextInput, TouchableOpacity, AsyncStorage} from 'react-native';
import GeneralStatusBarColor from '../../../components/generalStatusBar';
import SearchBar from '../../../components/searchBar';
import {Card, Text} from 'native-base';
import {userIdAtom, userTokenAtom} from '../../../recoil/atom';
import {useRecoilValue} from 'recoil';
import {transferStyle} from '../../../../assets/style/styles';
import {backGroundColor} from '../../../../assets/colors/colors';
import { API_URL } from '../../../../constants';

const HIEGHT = Dimensions.get('window').height;

const Reinvestment = ({navigation}) =>{
    const userId = useRecoilValue(userIdAtom)
    const token = useRecoilValue(userTokenAtom)
    const [amount, setAmount] = useState('')
    
    const sendMoney = () =>{
        if (amount < 100) {
            alert('Re-investement amount should be atleast 100 or greater!')
        }else {
            fetch(API_URL+'reinvest-money', {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                  userId: userId,
                  amount: amount,
                })
              })
        
                .then((response) => response.json())
                .then((responseData) => {
                    if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                        alert(responseJson.message)
                       AsyncStorage.clear()
                       navigation.navigate('login')
                    } else{
                  alert(JSON.stringify(responseData))
                }
                            
                })
                .catch((error) => {
                    alert(error)
                  setLoginLoading(false)
                })
        }
    }

    return(
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <GeneralStatusBarColor backgroundColor='#009387' fullName={'Reinvestment'} navigation={navigation} iconName='arrow-back-sharp'></GeneralStatusBarColor>
            {/* <SearchBar onChange={(val)=>_searchItem(val)} placeHolder='Search here...' value={search} ></SearchBar> */}

            <View style={{flex: 1, alignItems: 'center'}}>
                <Card style={[transferStyle.cardStyle, {height: HIEGHT/2.4}]}>
                    <View style={[transferStyle.titleView,{height: 120}]}>
                        <Text style={{margin: 20, fontWeight: 'bold', color: '#fff', textAlign: 'justify'}}>Transfer from main balance to incestments (Not reversible)
                        Multiple of 100's for Re-Investments Lower Limit is 50$ for Initial and 100$ for Re-Investments.</Text>
                    </View>
                    <View style={{flex: 1}}>
                        <Text style={{margin: 20}}>Amount</Text>
                        
                        <TextInput 
                        style={{width: '90%', height: 50, paddingLeft: 10, borderWidth: .5, alignSelf: 'center', borderRadius: 5, borderColor: '#ccc'}}
                        placeholder = 'Enter amount'
                        keyboardType='numeric'
                        onChangeText={(val)=>setAmount(val)}
                        ></TextInput>

                        <TouchableOpacity style={{width: 100, height: 50, backgroundColor: backGroundColor,
                             margin: 20, justifyContent: 'center', alignItems: 'center'}}
                            onPress={()=>sendMoney()}     
                        >
                            <Text style={{color: '#fff'}}>Send</Text>
                        </TouchableOpacity>
                    </View>
                </Card>

            </View>
        </View>
    )
}

export default Reinvestment;