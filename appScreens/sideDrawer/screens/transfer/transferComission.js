import React, {useState, useEffect} from 'react';
import {View, TouchableOpacity, AsyncStorage} from 'react-native';
import GeneralStatusBarColor from '../../../components/generalStatusBar';
import {Card, Text} from 'native-base';
import {userNameAtom, userIdAtom, userTokenAtom} from '../../../recoil/atom';
import {useRecoilValue} from 'recoil';
import {transferStyle, indexStyle} from '../../../../assets/style/styles';
import { API_URL } from '../../../../constants';

const TransferComission = ({navigation}) =>{
    const userId = useRecoilValue(userIdAtom)
    const userName = useRecoilValue(userNameAtom)
    const token = useRecoilValue(userTokenAtom)

    const [mainBalance, setMainBalance] = useState(0)
    const [wallet, setWallet] = useState(0)
    const [sale, setSale] = useState(0)
    const [comission, setComission] = useState(0)
    const [loading, setLoading] = useState(true)
    useEffect(()=>{
        getMainBalance()
        showSale()
        showComission()
    },[loading])

    const getMainBalance = () => {
        fetch(API_URL + 'get-user-main-balance/' + userId, {
            method: 'GET',
             headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                   AsyncStorage.clear()
                   navigation.navigate('login')
                } else if (responseJson.message === 'Record Found') {
                    setMainBalance(responseJson.data[0].pAds_mainBalance)
                    setWallet(responseJson.data[0].pAds_wallet)
                    setLoading(false)
                }
            })

            .catch((error) => {

            })
    }

    const showSale = () => {
        fetch(API_URL + 'get-total-sales-history/' + userName, {
            method: 'GET',
             headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                   AsyncStorage.clear()
                   navigation.navigate('login')
                } else if (responseJson.message === 'Record Found') {
                    setSale(responseJson.sales)
                }
            })

            .catch((error) => {

            })
    }

    const showComission = () => {
        fetch(API_URL + 'user-commissions/' + userId, {
            method: 'GET',
             headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
        })

            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                   AsyncStorage.clear()
                   navigation.navigate('login')
                } else if (responseJson.message === 'Record Found') {
                    setComission(responseJson.data)
                }
            })

            .catch((error) => {

            })
    }

    const transferComission = () =>{
        fetch(API_URL + 'transfer-commission-to-main-balance', {
            method: 'POST',
             headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token
            },
            body: JSON.stringify({
                userId: userId,
            })
        })

            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.success === false && responseJson.authMsg === 'token_error') {
                    alert(responseJson.message)
                   AsyncStorage.clear()
                   navigation.navigate('login')
                } else{
                    alert(JSON.stringify(responseJson))
                    getMainBalance()
                    showSale()
                    showComission()
                }

            })
            .catch((error) => {
            })
    }

    return(
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <GeneralStatusBarColor backgroundColor='#009387' fullName={'Transfer Comission'} navigation={navigation} iconName='arrow-back-sharp'></GeneralStatusBarColor>
            {/* <SearchBar onChange={(val)=>_searchItem(val)} placeHolder='Search here...' value={search} ></SearchBar> */}

            <View style={{flex: 1, alignItems: 'center'}}>
                <Card style={transferStyle.cardStyle}>
                    <View style={transferStyle.titleView}>
                        <Text style={{margin: 20, fontWeight: 'bold', color: '#fff'}}>My reward + salary balance</Text>
                    </View>
                    <View style={{flex: 1}}>
                        <Text style={transferStyle.titleStyle}>Reward + salary balance</Text>
                        <Text note style={{marginLeft: 20}}>
                        {loading}You have ${parseFloat(mainBalance).toFixed(2)} reward + salary balance
                        </Text>
                    </View>
                </Card>

                <Card style={transferStyle.cardStyle}>
                <View style={transferStyle.titleView}>
                        <Text style={{margin: 20, fontWeight: 'bold', color: '#fff'}}>Comission amount</Text>
                    </View>
                    <View style={{flex: 1}}>
                        <Text style={transferStyle.titleStyle}>Comission amount</Text>
                        <Text note style={{marginLeft: 20}}>You have ${parseFloat(sale).toFixed(2)} in sale and & {parseInt(comission.toFixed(2))} comission balance</Text>

                        {
                            comission <= 0 ? null : 
                            <TouchableOpacity style={[indexStyle.showTextBtn,
                             {alignSelf: 'flex-end', marginRight: 20, bottom: 0}]}
                                onPress={()=>transferComission()}
                             >
                                <Text style={{color: '#fff'}}>Transfer</Text>
                            </TouchableOpacity>
                        }

                    </View>
                </Card>

                <Card style={transferStyle.cardStyle}>
                <View style={transferStyle.titleView}>
                        <Text style={{margin: 20, fontWeight: 'bold', color: '#fff'}}>My earning + commission balance</Text>
                    </View>
                    <View style={{flex: 1}}>
                        <Text style={transferStyle.titleStyle}>Earning + comission balance</Text>
                        <Text note style={{marginLeft: 20}}> ${parseFloat(wallet).toFixed(2)}</Text>

                    </View>
                </Card>
            </View>
        </View>
    )
}

export default TransferComission;