import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import {createDrawerNavigator } from 'react-navigation-drawer'
import Login from './appScreens/user/login';
import {RecoilRoot} from 'recoil';
import SignUpScreen from './appScreens/user/signup';
import Splash from './appScreens/splish';
import SideDrawer from './appScreens/sideDrawer/drawer';
import Profile from './appScreens/sideDrawer/screens/dasboard/profile';
import MyEarnings from './appScreens/sideDrawer/screens/transaction/myEarnings';
import MySales from './appScreens/sideDrawer/screens/transaction/mySales';
import MyComission from './appScreens/sideDrawer/screens/transaction/myComission';
import MyLegalSale from './appScreens/sideDrawer/screens/transaction/myLegalSale';
import MergeAccount from './appScreens/sideDrawer/screens/account/mergeAccount';
import MergeRequest from './appScreens/sideDrawer/screens/account/mergeRequest';
import SendToMember from './appScreens/sideDrawer/screens/send/toMember';
import Exchanger from './appScreens/sideDrawer/screens/send/exchanger';
import TransferComission from './appScreens/sideDrawer/screens/transfer/transferComission';
import TransferEarning from './appScreens/sideDrawer/screens/transfer/transferEarning';
import Reinvestment from './appScreens/sideDrawer/screens/transfer/reInvestment';
import RewardHistory from './appScreens/sideDrawer/screens/reward/rewardHistory';
import MySalary from './appScreens/sideDrawer/screens/reward/mySalary';
import TransactionHistory from './appScreens/sideDrawer/screens/reports/transactionHistory';
import ReversalHistory from './appScreens/sideDrawer/screens/reports/reversalHistory';
import MyRewards from './appScreens/sideDrawer/screens/reward/myReward';
import DataTree from './appScreens/sideDrawer/screens/referral/tree';
import DataHeirarchy from './appScreens/sideDrawer/screens/referral/heirarchy';
import ForgotPassword from './appScreens/user/forgotPassword';
import Home from './appScreens/home/index';
import MyMine from './appScreens/home/myMine';
import ChangePassword from './appScreens/sideDrawer/screens/dasboard/changePassword';
import MemberPlusExchanger from './appScreens/sideDrawer/screens/send/memberPlusExchanger';
import EarningCommissionHistory from './appScreens/sideDrawer/screens/reports/earningCommissionHistory';


AppNavigation = createStackNavigator({
  splish: {
    screen: Splash,
    navigationOptions: {
      headerShown: false
    }
  },
  login: {
    screen: Login,
    navigationOptions: {
      headerShown: false
    }
  },
  signup: {
    screen: SignUpScreen,
    navigationOptions: {
      headerShown: false
    }
  },
  home: {
    screen: Home,
    navigationOptions: {
      headerShown: false
    }
  },
  profile: {
    screen: Profile,
    navigationOptions: {
      headerShown: false
    }
  },
  myEarnings: {
    screen: MyEarnings,
    navigationOptions: {
      headerShown: false
    }
  },
  mySales: {
    screen: MySales,
    navigationOptions: {
      headerShown: false
    }
  },
  myComission: {
    screen: MyComission,
    navigationOptions: {
      headerShown: false
    }
  },
  legalSale: {
    screen: MyLegalSale,
    navigationOptions: {
      headerShown: false
    }
  },
  mergeAccount: {
    screen: MergeAccount,
    navigationOptions: {
      headerShown: false
    }
  },
  mergeRequest: {
    screen: MergeRequest,
    navigationOptions: {
      headerShown: false
    }
  },
  sendTo: {
    screen: SendToMember,
    navigationOptions: {
      headerShown: false
    }
  },
  exchanger: {
    screen: Exchanger,
    navigationOptions: { 
      headerShown: false
    }
  },
  memberPlusExchanger: {
    screen: MemberPlusExchanger,
    navigationOptions: {
      headerShown: false
    }
  },
  tranferComission: {
    screen: TransferComission,
    navigationOptions: {
      headerShown: false
    }
  },
  transferEarning: {
    screen: TransferEarning,
    navigationOptions: {
      headerShown: false
    }
  },
  reinvestment: {
    screen: Reinvestment,
    navigationOptions: {
      headerShown: false
    }
  },
  myReward: {
    screen: MyRewards,
    navigationOptions: {
      headerShown: false
    }
  },
  rewardHistory: {
    screen: RewardHistory,
    navigationOptions: {
      headerShown: false
    }
  },
  mySalary: {
    screen: MySalary,
    navigationOptions: {
      headerShown: false
    }
  },
  transactionHistory: {
    screen: TransactionHistory,
    navigationOptions: {
      headerShown: false
    }
  },
  reversalHistory: {
    screen: ReversalHistory,
    navigationOptions: {
      headerShown: false
    }
  },
  earningCommissionHistory:{
    screen: EarningCommissionHistory,
    navigationOptions: {
      headerShown: false
    }
  },
  dataTree: {
    screen: DataTree,
    navigationOptions: {
      headerShown: false
    }
  },
  // dataHeirarchy: {
  //   screen: DataHeirarchy,
  //   navigationOptions: {
  //     headerShown: false
  //   }
  // },
  myMine: {
    screen: MyMine,
    navigationOptions: {
      headerShown: false
    }
  },
  forgot: {
    screen: ForgotPassword,
    navigationOptions: {
      headerShown: false
    }
  },
  changePassword: {
    screen: ChangePassword,
    navigationOptions: {
      headerShown: false
    }
  }
})

// const Appcontainer = createAppContainer(AppNavigation);
const Appcontainer = createAppContainer(
  createDrawerNavigator({
    navigation:{screen:AppNavigation}
  },{
    contentComponent: SideDrawer,
    drawerBackgroundColor:'#fff'
  })
)
export default class App extends React.Component {
  render() {
    return (
      <RecoilRoot>
        <Appcontainer></Appcontainer>
      </RecoilRoot>
    );
  }
}